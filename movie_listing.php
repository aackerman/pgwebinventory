<html>  
  <head>
    <title>PgWebInventory 2.1 - Movie Listing</title>
  </head>
  <body>
    <CENTER><H1>PgWebInventory - Movie Listing</H1></Center>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      switch ($format) {
        case 'all':  // Show all movies in all formats
          print "<B>All</B> Movies<BR>\n";
          $query = "SELECT * FROM movie_listing_view";
          break;
        case 'dvd':  // Show only DVD movies
          print "<B>DVD</B> Movies<BR>\n";
          $query = "SELECT mov_index AS \"Movie ID\", name AS \"Name\"," .
                   "       format AS \"Movie Format\", rating AS \"Movie Rating\", " .
                   "       year AS \"Year\" " .
                   "FROM movie, movie_format, movie_rating " .
                   "WHERE (movie.form_index = movie_format.form_index) AND " .
                   "      (movie.rat_index = movie_rating.rat_index) AND " .
                   "      (movie_format.format = 'DVD') " .
                   "ORDER BY name";
          break;
        case 'vhs':  // Show only VHS tapes
          print "<B>VHS</B> Movies<BR>\n";
          $query = "SELECT mov_index AS \"Movie ID\", name AS \"Name\", " .
                   "       format AS \"Movie Format\", rating AS \"Movie Rating\", " .
                   "       year AS \"Year\" " .
                   "FROM movie, movie_format, movie_rating " .
                   "WHERE (movie.form_index = movie_format.form_index) AND " .
                   "      (movie.rat_index = movie_rating.rat_index) AND " .
                   "      (movie_format.format = 'VHS') " .
                   "ORDER BY name";
          break;
        case 'beta':  // Show only BetaMax tapes
          print "<B>Beta</B> Movies<BR>\n";
          $query = "SELECT mov_index AS \"Movie ID\", name AS \"Name\", " .
                   "       format AS \"Movie Format\", rating AS \"Movie Rating\", " .
                   "       year AS \"Year\" " .
                   "FROM movie, movie_format, movie_rating " .
                   "WHERE (movie.form_index = movie_format.form_index) AND " .
                   "      (movie.rat_index = movie_rating.rat_index) AND " .
                   "      (movie_format.format = 'Beta') " .
                   "ORDER BY name";
          break;
        default:  // Huh?
          print "Unknown option.  Please specify a movie format option.<BR>\n";
          exit;
      }  // Switch Statement - Which Format to Display? 
      $result = pg_query($database, $query);
      if ( ! $result ) {
        $dberror = pg_last_error($database);
        return false;
      }  // IF STATEMENT
      $nummovies = pg_num_rows($result);
      print "There are $nummovies movies in the Inventory.<br>\n";
      print "<TABLE BORDER=1>\n";
      print "<TR>";
      for ($j = 1; $j < pg_num_fields($result); $j++) {
        print "<TH>". pg_field_name($result,$j). "</TH>";
      }
      print "</TR>\n";
      for ($i = 0; $row=@pg_fetch_array($result,$i); $i++) { 
        print "<TR>\n";
        print "<TD><A HREF=\"movie_retrieve.php?number=$row[0]\">$row[1]</A></TD>" .
              "<TD>$row[2]</TD><TD>$row[3]</TD><TD>$row[4]</TD>";
        print "</TR>\n";
      } // FOR STATEMENT
      print "</TABLE>\n";

    ?>
    <BR><HR><BR>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="movie_listing.php?format=all">View List of All Movies in the Inventory</A><BR>
    <A HREF="movie_entry.php">Enter a New Movie into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
