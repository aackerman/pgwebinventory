<html>  
  <head>
    <title>PgWebInventory 2.1 - Music Listing</title>
  </head>
  <body>
    <CENTER><H1>PgWebInventory - Music Listing</H1></Center>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      $query = "SELECT * FROM music_listing_view";
      $result = pg_query($database, $query);
      if ( ! $result ) {
        $dberror = pg_last_error($database);
        return false;
      }  // IF STATEMENT
      $nummusic = pg_num_rows ($result);
      print "There are <B>$nummusic</B> items in the Music Inventory.<BR><BR>\n";
      print "<TABLE BORDER=1>\n";
      print "<TR>";
      for ($j = 1; $j < pg_num_fields($result); $j++) {
        print "<TH>". pg_field_name($result,$j). "</TH>";
      }
      print "</TR>\n";
      for ($i = 0; $row=@pg_fetch_array($result,$i); $i++) 
        print "  <TR><TD>$row[1]</TD>" .
              "<TD><A HREF=\"music_retrieve.php?number=$row[0]\">$row[2]</A></TD>" .
              "<TD>$row[3]</TD></TR>\n";
      print "</TABLE>\n";
    ?>
    <BR><HR><BR>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="music_listing.php">View List of All Albums in the Inventory</A><BR>
    <A HREF="music_entry.php">Enter Music into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
