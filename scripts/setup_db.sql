-------------------------------------------------------------------------------
--                 PgWebInventory 2.0.0 Initial SQL Setup                    --
--                                                                           --
-- Run using:                                                                --
-- $ psql -f setup_db.sql                                                    --
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- SECTION 1:  Administrivia                                                 --
-------------------------------------------------------------------------------
-- Create the Home Inventory database.  
-- Make sure this matches what you have in "config.php" as the database.
CREATE DATABASE inventory;

-- Connect to the database
\c inventory

-- Un-comment this line if you have not created the apache user in PostgreSQL.
-- This is required because the program is not written to connect using a 
-- specific userid.  It will connect using the working user, which in the case 
-- of the Apache Web Server, is normally "apache".  Change this userid if your
-- setup is different.
CREATE USER apache;

-- Un-comment this line if you would like your userid to have permissions on 
-- the db.  Otherwise, you will have to use the "postgres" userid if you want
-- to do any db maintenance.  REMEMBER TO CHANGE THIS TO MATCH YOUR USERID.  
-- Also, you will have to change the userid to permission settings below in 
-- SECTION 3.
CREATE USER alex;

-------------------------------------------------------------------------------
-- SECTION 2:  Table Creation                                                --
-------------------------------------------------------------------------------

-- Create the tables for the Book database
CREATE TABLE Book ( 
	book_index  int4 PRIMARY KEY, 
	ISBN  varchar(16), 
	Title  varchar(100), 
	pub_index  int4, 
	year  int4, 
	form_index  int4, 
	Edition  int4, 
	series_index  int4, 
	Volume  int4 DEFAULT '0', 
	Notes  text
); 
CREATE TABLE Book_Author ( 
	auth_index  serial, 
	Author  varchar(50) UNIQUE, 
	Notes  text
); 
CREATE TABLE Book_Category ( 
	cat_index  serial, 
	Category  varchar(80) UNIQUE
); 
CREATE TABLE Book_Format (
	form_index  serial,
	Format  varchar(9) UNIQUE
); 
CREATE TABLE Book_Image (
	id serial,
	book_index int4,
	image OID
);
CREATE TABLE Book_Publisher (
	pub_index  serial,
	Publisher  varchar(50) UNIQUE, 
	Notes  text
); 
CREATE TABLE Book_Series (
	series_index  serial,
	series  varchar(50) UNIQUE
); 
CREATE TABLE book_auth_idx (
	book_idx  int4 NOT NULL,
	auth_idx  int4 NOT NULL
); 
CREATE TABLE book_cat_idx (
	book_idx  int4 NOT NULL,
	book_cat_idx  int4 NOT NULL
); 

-- Create the tables for the movie database
CREATE TABLE Movie (
	mov_index int4 PRIMARY KEY,
	name varchar(70),
	year int4,
	rat_index int4,
	length int4,
	form_index int4,
	synopsis text
);
CREATE TABLE Movie_Actor (
	act_index serial,
	actor varchar(50) UNIQUE,
	notes text
);
CREATE TABLE Movie_Category (
	cat_index serial,
	category  varchar(20) UNIQUE
); 
CREATE TABLE Movie_Director (
	dir_index serial,
	director  varchar(50) UNIQUE,
	notes  text
); 
CREATE TABLE Movie_Format (
	form_index serial,
	format varchar(4) UNIQUE
);
CREATE TABLE Movie_Image (
	id serial,
	mov_index int4,
	image OID
);
CREATE TABLE Movie_Rating (
	rat_index serial,
	rating  varchar(7) UNIQUE
); 
CREATE TABLE Movie_Screen_Format (
	scr_index serial,
	screen_format varchar (30) UNIQUE
);
CREATE TABLE Movie_Sound (
	sound_index serial,
	sound_option varchar (20) UNIQUE
);
CREATE TABLE DVD_Aspect_Ratio (
	asp_index serial,
	aspect_ratio varchar(30) UNIQUE
);
CREATE TABLE DVD_Languages (
	lang_index serial,
	language varchar(20) UNIQUE
);
CREATE TABLE DVD_Special_Features (
	sf_index serial,
	special_feature varchar(40) UNIQUE
);
CREATE TABLE DVD_Subtitles (
	sub_index serial,
	subtitle varchar(20) UNIQUE
);
CREATE TABLE mov_act_idx (
	mov_idx int4 NOT NULL,
	act_idx int4 NOT NULL
);
CREATE TABLE mov_ar_idx (
	mov_idx int4 NOT NULL,
	ar_idx int4 NOT NULL
);
CREATE TABLE mov_cat_idx (
	mov_idx int4 NOT NULL,
	cat_idx int4 NOT NULL
);
CREATE TABLE mov_dir_idx (
	mov_idx int4 NOT NULL,
	dir_idx int4 NOT NULL
);
CREATE TABLE mov_lang_idx (
	mov_idx int4 NOT NULL,
	lang_idx int4 NOT NULL
);
CREATE TABLE mov_scr_idx (
	mov_idx int4 NOT NULL,
	scr_idx int4 NOT NULL
);
CREATE TABLE mov_sf_idx (
	mov_idx int4 NOT NULL,
	sf_idx int4 NOT NULL
);
CREATE TABLE mov_snd_idx (
	mov_idx int4 NOT NULL,
	snd_idx int4 NOT NULL
);
CREATE TABLE mov_sub_idx (
	mov_idx int4 NOT NULL,
	sub_idx int4 NOT NULL
);

-- Create the tables for the Music database
CREATE TABLE Music (
	mus_index  int4 PRIMARY KEY,
	title  varchar(60),
	year  int4,
	lab_index  int4,
	num_tracks  int4,
	form_index  int4,
	notes  text
); 
CREATE TABLE Music_Artist (
	art_index  serial,
	artist  varchar(50) UNIQUE,
	notes  text
); 
CREATE TABLE Music_Category (
	cat_index serial,
	category  varchar(20) UNIQUE
); 
CREATE TABLE Music_Format (
	form_index serial,
	format  varchar(12) UNIQUE
); 
CREATE TABLE Music_Image (
	id serial,
	mus_index int4,
	image OID
);
CREATE TABLE Music_Label (
	lab_index serial,
	recording_label  varchar(50) UNIQUE,
	notes  Text
); 
CREATE TABLE mus_art_idx (
	mus_idx int4 NOT NULL,
	art_idx int4 NOT NULL
);
CREATE TABLE mus_cat_idx (
	mus_idx int4 NOT NULL,
	cat_idx int4 NOT NULL
);

-------------------------------------------------------------------------------
-- SECTION 3:  Permission Settings                                           --
-------------------------------------------------------------------------------

-- Grant permissions to the database administrator (if you are using another 
-- account other than postgres) and to the web browser user.  Make sure these 
-- match the users you created in the database in SECTION 1.
GRANT ALL ON book TO alex;
GRANT ALL ON book_auth_idx TO alex;
GRANT ALL ON book_author TO alex;
GRANT ALL ON book_author_auth_index_seq TO alex;
GRANT ALL ON book_cat_idx TO alex;
GRANT ALL ON book_category TO alex;
GRANT ALL ON book_category_cat_index_seq TO alex;
GRANT ALL ON book_format TO alex;
GRANT ALL ON book_format_form_index_seq TO alex;
GRANT ALL ON book_image TO alex;
GRANT ALL ON book_image_id_seq TO alex;
GRANT ALL ON book_publisher TO alex;
GRANT ALL ON book_publisher_pub_index_seq TO alex;
GRANT ALL ON book_series TO alex;
GRANT ALL ON book_series_series_index_seq TO alex;

GRANT ALL ON movie TO alex;
GRANT ALL ON movie_actor TO alex;
GRANT ALL ON movie_actor_act_index_seq TO alex;
GRANT ALL ON movie_category TO alex;
GRANT ALL ON movie_category_cat_index_seq TO alex;
GRANT ALL ON movie_director TO alex;
GRANT ALL ON movie_director_dir_index_seq TO alex;
GRANT ALL ON movie_format TO alex;
GRANT ALL ON movie_format_form_index_seq TO alex;
GRANT ALL ON movie_image TO alex;
GRANT ALL ON movie_image_id_seq TO alex;
GRANT ALL ON movie_rating TO alex;
GRANT ALL ON movie_rating_rat_index_seq TO alex;
GRANT ALL ON movie_screen_form_scr_index_seq TO alex;
GRANT ALL ON movie_screen_format TO alex;
GRANT ALL ON movie_sound TO alex;
GRANT ALL ON movie_sound_sound_index_seq TO alex;
GRANT ALL ON dvd_aspect_ratio TO alex;
GRANT ALL ON dvd_aspect_ratio_asp_index_seq TO alex;
GRANT ALL ON dvd_languages TO alex;
GRANT ALL ON dvd_languages_lang_index_seq TO alex;
GRANT ALL ON dvd_special_featur_sf_index_seq TO alex;
GRANT ALL ON dvd_special_features TO alex;
GRANT ALL ON dvd_subtitles TO alex;
GRANT ALL ON dvd_subtitles_sub_index_seq TO alex;
GRANT ALL ON mov_act_idx TO alex;
GRANT ALL ON mov_ar_idx TO alex;
GRANT ALL ON mov_cat_idx TO alex;
GRANT ALL ON mov_dir_idx TO alex;
GRANT ALL ON mov_lang_idx TO alex;
GRANT ALL ON mov_scr_idx TO alex;
GRANT ALL ON mov_sf_idx TO alex;
GRANT ALL ON mov_snd_idx TO alex;
GRANT ALL ON mov_sub_idx TO alex;

GRANT ALL ON music TO alex;
GRANT ALL ON music_artist TO alex;
GRANT ALL ON music_artist_art_index_seq TO alex;
GRANT ALL ON music_category TO alex;
GRANT ALL ON music_category_cat_index_seq TO alex;
GRANT ALL ON music_format TO alex;
GRANT ALL ON music_format_form_index_seq TO alex;
GRANT ALL ON music_image TO alex;
GRANT ALL ON music_image_id_seq TO alex;
GRANT ALL ON music_label TO alex;
GRANT ALL ON music_label_lab_index_seq TO alex;
GRANT ALL ON mus_art_idx TO alex;
GRANT ALL ON mus_cat_idx TO alex;

GRANT SELECT,INSERT,UPDATE ON book TO apache;
GRANT SELECT,INSERT,UPDATE ON book_author TO apache;
GRANT SELECT,INSERT,UPDATE ON book_author_auth_index_seq TO apache;
GRANT SELECT,INSERT,UPDATE ON book_category TO apache;
GRANT SELECT,INSERT,UPDATE ON book_category_cat_index_seq TO apache;
GRANT SELECT,INSERT,UPDATE ON book_format TO apache;
GRANT SELECT,INSERT,UPDATE ON book_format_form_index_seq TO apache;
GRANT SELECT,INSERT,UPDATE ON book_image TO apache;
GRANT SELECT,INSERT,UPDATE ON book_image_id_seq TO apache;
GRANT SELECT,INSERT,UPDATE ON book_publisher TO apache;
GRANT SELECT,INSERT,UPDATE ON book_publisher_pub_index_seq TO apache;
GRANT SELECT,INSERT,UPDATE ON book_series TO apache;
GRANT SELECT,INSERT,UPDATE ON book_series_series_index_seq TO apache;
GRANT SELECT,INSERT,UPDATE ON book_auth_idx TO apache;
GRANT SELECT,INSERT,UPDATE ON book_cat_idx TO apache;

GRANT SELECT, INSERT, UPDATE ON movie TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_actor TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_actor_act_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_category TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_category_cat_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_director TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_director_dir_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_format TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_format_form_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_image TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_image_id_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_rating TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_rating_rat_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_screen_form_scr_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_screen_format TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_sound TO apache;
GRANT SELECT, INSERT, UPDATE ON movie_sound_sound_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_aspect_ratio TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_aspect_ratio_asp_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_languages TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_languages_lang_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_special_featur_sf_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_special_features TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_subtitles TO apache;
GRANT SELECT, INSERT, UPDATE ON dvd_subtitles_sub_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_act_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_ar_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_cat_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_dir_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_lang_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_scr_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_sf_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_snd_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mov_sub_idx TO apache;

GRANT SELECT, INSERT, UPDATE ON music TO apache;
GRANT SELECT, INSERT, UPDATE ON music_artist TO apache;
GRANT SELECT, INSERT, UPDATE ON music_artist_art_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON music_category TO apache;
GRANT SELECT, INSERT, UPDATE ON music_category_cat_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON music_format TO apache;
GRANT SELECT, INSERT, UPDATE ON music_format_form_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON music_image TO apache;
GRANT SELECT, INSERT, UPDATE ON music_image_id_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON music_label TO apache;
GRANT SELECT, INSERT, UPDATE ON music_label_lab_index_seq TO apache;
GRANT SELECT, INSERT, UPDATE ON mus_art_idx TO apache;
GRANT SELECT, INSERT, UPDATE ON mus_cat_idx TO apache;

-- Seed the database
\i ./seed_db.sql
