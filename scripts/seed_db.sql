-------------------------------------------------------------------------------
--                 PgWebInventory 2.0.0 Initial Database Seed                --
--                                                                           --
-- This script will insert into the database the basic seed data for the     --
-- user.  I have tried to come up with some basic authors, actors, ratings,  --
-- etc.  Please feel free to let me know if I should include any others in   --
-- this seed file.                                                           --
--                                                                           --
-- Run using:                                                                --
-- $ psql -f setup_db.sql                                                    --
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- SECTION 1:  Book Data                                                     --
-------------------------------------------------------------------------------
-- Insert a couple of authors...
INSERT INTO book_author (author, notes) VALUES ('Brooks, Terry','');
INSERT INTO book_author (author, notes) VALUES ('Clancy, Tom','');
INSERT INTO book_author (author, notes) VALUES ('Gibson, William','');

-- Insert a couple of categories...
INSERT INTO book_category (category) VALUES ('Computer - Programming - Languages - C/C++');
INSERT INTO book_category (category) VALUES ('Computer - Operating Systems - Linux');
INSERT INTO book_category (category) VALUES ('Mysteries and Thrillers - Thriller - Technothriller');

-- Insert a couple of publishers...
INSERT INTO book_publisher (publisher, notes) VALUES ('Prentice Hall PTR','');
INSERT INTO book_publisher (publisher, notes) VALUES ('McGraw Hill, Inc','');
INSERT INTO book_publisher (publisher, notes) VALUES ('Berkley Books','');

-- Insert the book formats...
INSERT INTO book_format (format) VALUES ('Hardcover');
INSERT INTO book_format (format) VALUES ('Paperback');
INSERT INTO book_format (format) VALUES ('Trade Paperback');

-- Insert a couple of book series...
-- This first one must be in the database as-is.  
INSERT INTO book_series (series) VALUES ('0 - No SERIES');
INSERT INTO book_series (series) VALUES ('Tom Clancy\'s Net Force');
INSERT INTO book_series (series) VALUES ('Tom Clancy\'s Op-Center');
INSERT INTO book_series (series) VALUES ('Tom Clancy\'s Power Plays');

-------------------------------------------------------------------------------
-- SECTION 2:  Movie Data                                                    --
-------------------------------------------------------------------------------
-- Insert a couple directors...
INSERT INTO movie_director (director, notes) VALUES ('Softley, Iain','');
INSERT INTO movie_director (director, notes) VALUES ('Levinson, Barry','');
INSERT INTO movie_director (director, notes) VALUES ('Glen, John','');

-- Insert a couple actors...
INSERT INTO movie_actor (actor, notes) VALUES ('Miller, Jonny Lee','');
INSERT INTO movie_actor (actor, notes) VALUES ('Jolie, Angelina','');
INSERT INTO movie_actor (actor, notes) VALUES ('Neeson, Liam','');

-- Insert a couple categories...
INSERT INTO movie_category (category) VALUES ('Horror');
INSERT INTO movie_category (category) VALUES ('Action/Adventure');
INSERT INTO movie_category (category) VALUES ('Science Fiction');

-- Insert the screen formats...
INSERT INTO movie_screen_format (screen_format) VALUES ('Widescreen');
INSERT INTO movie_screen_format (screen_format) VALUES ('Full Screen');

-- Insert the movie ratings...
INSERT INTO movie_rating (rating) VALUES ('G');
INSERT INTO movie_rating (rating) VALUES ('PG');
INSERT INTO movie_rating (rating) VALUES ('PG-13');
INSERT INTO movie_rating (rating) VALUES ('R');
INSERT INTO movie_rating (rating) VALUES ('NC-17');
INSERT INTO movie_rating (rating) VALUES ('NR');

-- Insert the movie formats.  DO NOT DELETE FROM THIS LIST
INSERT INTO movie_format (format) VALUES ('VHS');
INSERT INTO movie_format (format) VALUES ('DVD');
INSERT INTO movie_format (format) VALUES ('Beta');
INSERT INTO movie_format (format) VALUES ('LDsc');

-- Insert the movie sound options...
INSERT INTO movie_sound (sound_option) VALUES ('Dolby Digital');
INSERT INTO movie_sound (sound_option) VALUES ('Dolby Surround');
INSERT INTO movie_sound (sound_option) VALUES ('THX');

-- Insert a couple of DVD special features...
INSERT INTO dvd_special_features (special_feature) VALUES ('Behind the Scenes Featurettes');
INSERT INTO dvd_special_features (special_feature) VALUES ('Deleted Scenes');
INSERT INTO dvd_special_features (special_feature) VALUES ('Music Video');

-- Insert a couple of DVD language options...
INSERT INTO dvd_languages (language) VALUES ('English');
INSERT INTO dvd_languages (language) VALUES ('French');
INSERT INTO dvd_languages (language) VALUES ('German');

-- Insert a couple of DVD subtitle options...
INSERT INTO dvd_subtitles (subtitle) VALUES ('English');
INSERT INTO dvd_subtitles (subtitle) VALUES ('French');
INSERT INTO dvd_subtitles (subtitle) VALUES ('Spanish');

-- Insert a couple of aspect ratios...
INSERT INTO dvd_aspect_ratio (aspect_ratio) VALUES ('1.66 : 1');
INSERT INTO dvd_aspect_ratio (aspect_ratio) VALUES ('2.35 : 1');

-------------------------------------------------------------------------------
-- SECTION 3:  Music Data                                                    --
-------------------------------------------------------------------------------
-- Insert a couple of music artists...
INSERT INTO music_artist (artist, notes) VALUES ('Pink Floyd','The greatest band of all time!');
INSERT INTO music_artist (artist, notes) VALUES ('Daft Punk','');
INSERT INTO music_artist (artist, notes) VALUES ('David, Craig','');

-- Insert a couple of categories...
INSERT INTO music_category (category) VALUES ('Classic Rock');
INSERT INTO music_category (category) VALUES ('Pop');
INSERT INTO music_category (category) VALUES ('Heavy Metal');

-- Insert a couple of recording labels...
INSERT INTO music_label (recording_label, notes) VALUES ('Capital Records','');
INSERT INTO music_label (recording_label, notes) VALUES ('EMI','');
INSERT INTO music_label (recording_label, notes) VALUES ('Sony Music','');

-- Insert the music formats...
INSERT INTO music_format (format) VALUES ('Cassette');
INSERT INTO music_format (format) VALUES ('Compact Disk');
INSERT INTO music_format (format) VALUES ('Minidisk');
INSERT INTO music_format (format) VALUES ('LP');

