#!/bin/sh
#
SED=`which sed`
echo "Using $SED as our editor"
echo -n "Please enter the name of the database to create: "
read db
echo -n "Please enter the DBA for the database: "
read dba
echo "The DBA for the database $db is:  $dba"

# Update the initial setup script to annotate the DBA for the
# database
echo "Updating SQL scripts..."
$SED "s/alex/$dba/" ./setup_db.sq_ > ./setup.sql
$SED "s/db_invent/$db/" ./setup.sql > ./setup_db.sql
chown apache.apache ./setup_db.sql

echo "Updating Configuration..."
$SED "s/db_invent/$db/" ../config/config.ph_ > ../config/config.php
chown apache.apache ../config/config.php
chmod 644 ../config/config.php

# Change user, change to separate directory, create something and exit
echo "Creating Home Inventory Database structure..."
touch ./log.txt
chmod 666 ./log.txt
su postgres -c ./postgres.sh 

# Clean up
echo "Cleaning up..."
rm -f ./setup.sql ./setup_db.sq_ ../config/config.ph_
chmod 644 ./log.txt

echo "Install complete.  Please read log.txt in this directory for any errors."
