<html>  
  <head>
    <title>PgWebInventory 2.1 - Music Entry Form</title>
  </head>
  <body>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      if (isset($slacker)) { 
        // Check user input here!
        $dberror = "";
        $index = "";
        $return = add_to_database ($index, $artindex, $title, $catindex, $year,
                                   $labindex, $numtracks, $formindex , $notes,
                                   $userfile, $dberror);
        if (! $return)
          print "Error: $dberror<BR>";
        else
          print "Thank you very much.  " . $title . " added.<BR>";
        } // IF STATEMENT
      else {
        write_form();
      }  // IF STATEMENT
    
     function add_to_database ($index, $artindex, $title, $catindex, $year,
                               $labindex, $numtracks, $formindex , $notes,
                               $userfile, $dberror) {

        GLOBAL $database, $filename, $_FILES;

        // Generate Music Index Value
        $result = pg_query($database,"SELECT * from music");
        $index = pg_num_rows($result) + 1;

        // Insert New Music
        $query = "INSERT INTO music VALUES ($index, '$title', $year, $labindex, $numtracks, $formindex , '$notes')";
        $query_a = "INSERT INTO mus_art_idx VALUES ($index, $artindex)";
        $query_b = "INSERT INTO mus_cat_idx VALUES ($index, $catindex)";
        if ( ! pg_query($database, $query) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_a) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_b) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT

        // Is there an Album cover to upload?
        $uploadedFile = $_FILES['userfile']['tmp_name'];
        $realName = $_FILES['userfile']['name'];
        if (is_uploaded_file($userfile)) {
          pg_query ($database, "BEGIN");
          $tmpfname = "/tmp/uploadFile";
          move_uploaded_file ($userfile, "$tmpfname");
          $foid = pg_lo_import ($database, $tmpfname);
          $imgquery = "INSERT INTO music_image (mus_index, image) VALUES ($index, $foid)";
          if ( ! pg_query($database, $imgquery) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          pg_query ($database, "COMMIT");
          unlink ($tmpfname);
        }

        $fp = fopen($filename,'a+');
        fwrite($fp,"$query ;\n");
        fwrite($fp,"$query_a ;\n");
        fwrite($fp,"$query_b ;\n");
        if ($imgquery == '') {
        } else {
            fwrite($fp,"$imgquery ;\n");
        }
        fclose($fp);
        return true;
      }  // FUNCTION:  add_to_database

      function write_form() {

        GLOBAL $database, $PHP_SELF;

        print "<CENTER><H1>PgWebInventory - Music Entry Form</H1></Center>\n";
        print "<FORM ENCTYPE=\"multipart/form-data\" ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
        print "Artist: <SELECT NAME=\"artindex\">\n";
        $result = pg_query ($database, "SELECT * FROM music_artist ORDER BY artist");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=2&varname=Artist&table=music_artist\">Enter New Music Artist</A><BR>\n";
        print "Title: <INPUT TYPE=\"text\" NAME=\"title\" SIZE=60><BR>\n";
        print "Category: <SELECT NAME=\"catindex\">\n";
        $result = pg_query ($database, "SELECT * FROM music_category ORDER BY category");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Category&table=music_category\">Enter New Music Category</A><BR>\n";
        print "Year: <INPUT TYPE=\"text\" NAME=\"year\" SIZE=4><BR>\n";
        print "Recording Label: <SELECT NAME=\"labindex\">\n";
        $result = pg_query ($database, "SELECT * FROM music_label ORDER BY recording_label");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=2&varname=Recording_Label&table=music_label\">Enter New Music Recording Label</A><BR>\n";
        print "Number of Tracks: <INPUT TYPE=\"text\" NAME=\"numtracks\" SIZE=2><BR>\n";
        print "Music Format: <SELECT NAME=\"formindex\">\n";
        $result = pg_query ($database, "SELECT * FROM music_format ORDER BY format");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Format&table=music_format\">Enter New Music Format</A><BR>\n";
        
      ?>

      Notes: <BR><TEXTAREA name="notes" COLS=60 ROWS=6 WRAP=physical></TEXTAREA><BR>
      <INPUT TYPE="hidden" NAME="MAX_FILE_SIZE" VALUE=500000>
      Image of Album Cover (optional):  <INPUT TYPE="file" NAME="userfile"><BR>
      <input type="hidden" name="slacker" value="Y">
      <input type="submit" value="Insert Album into Inventory">
      <input type="reset" value="Reset Form">
    </form>

    <?php
      }  // FUNCTION: write_form

    ?>
    <BR><HR><BR>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="music_listing.php">View List of All Music in the Inventory</A><BR>
    <A HREF="music_entry.php">Enter Music into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
