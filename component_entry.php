<html>  
  <head>
    <title>PgWebInventory 2.1 - Component Entry Form</title>
  </head>
  <body>
    <?php
      require ("./config/config.php"); 
      if (isset($slacker)) { 
        // Check user input here!
        $dberror = "";
        $index = "";
        switch ($ctype) {
          case 1:
            $return = add_to_database_1 ($table, $item1, $dberror);
            break;
          case 2:
            $return = add_to_database_2 ($table, $item1, $item2, $dberror);
            break;
          default:
            break;
        }  // SWITCH STATEMENT
        if (! $return)
          print "Error: $dberror<BR>";
        else
          print "Thank you very much.  $item1 ($varname) added.<BR>";
        } // IF STATEMENT
      else {
        write_form($table, $ctype, $varname);
      }  // IF STATEMENT
    
      function write_form($table, $ctype, $varname) {
        global $PHP_SELF;
        print "<CENTER><H1>PgWebInventory - Component Entry Form</H1></Center>\n";
        print "<form action=\"$PHP_SELF\" method=\"POST\">\n";
        print "This form is used to enter the drop-down data you will need to enter movies.  When entering names, please enter the name \"Last, First\" without the quotes.  <BR><BR>\n";
        print "$varname: <input type=\"text\" name=\"item1\"><BR>\n";
        switch ($ctype) {
          case 1:
            break;
          case 2:
            print "Notes: <BR><TEXTAREA name=\"item2\" COLS=60 ROWS=6 WRAP=physical></TEXTAREA><BR>\n";
            break;
        }  // SWITCH STATEMENT
        print "<input type=\"hidden\" name=\"table\" value=\"$table\">\n";
        print "<input type=\"hidden\" name=\"ctype\" value=\"$ctype\">\n";
        print "<input type=\"hidden\" name=\"varname\" value=\"$varname\">\n";
        print "<input type=\"hidden\" name=\"slacker\" value=\"Y\"><BR>\n";
        print "<input type=\"submit\" value=\"Insert Component\">\n";
        print "<input type=\"reset\" value=\"Reset Form\">\n";
        print "</form>\n";
      }  // FUNCTION: write_form

      print "<BR><HR><BR>\n";
      print "<A HREF=\"index.php\">Return to Main Page</A><BR>\n";
      print "<A HREF=\"component_entry.php?ctype=$ctype&varname=$varname&table=$table\">Enter a New Component into the Inventory</A><BR>\n";

  include("overall_footer.php"); 
?>
