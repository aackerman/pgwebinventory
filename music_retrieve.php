<HTML>
  <HEAD>
    <TITLE>PgWebInventory 2.1 - Music Retrieve</TITLE>
  </HEAD>
  <BODY>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      if (isset($number)) 
        retrieve_music ($number, $withimg);
      else 
        write_form();

      function retrieve_music ($number, $withimg) {

        GLOBAL $database;

        print "<CENTER><H1>PgWebInventory - Music Retrieval Results</H1></CENTER>\n";
        print "<BR><HR><BR>\n";
        if ($number == 'dump') { 
          $res_a = pg_query ($database, "SELECT mus_index, title FROM music ORDER BY title");
          for ($i = 0; $dRow=@pg_fetch_array($res_a, $i); $i++) {
            $dumpQuery = "SELECT mus_index, artist, title, category, year, " .
                         "recording_label, num_tracks, format, music.notes " . 
                         "FROM music, music_artist, music_category, " .
                         "music_label, music_format, mus_art_idx, mus_cat_idx " . 
                         "WHERE ((mus_index = $dRow[0]) AND " .
                         "(mus_art_idx.art_idx = music_artist.art_index) AND " .
                         "(music.mus_index = mus_art_idx.mus_idx) AND " .
                         "(music.lab_index = music_label.lab_index) AND " . 
                         "(music.form_index = music_format.form_index) AND " .
                         "(music.mus_index = mus_cat_idx.mus_idx) AND " .
                         "(music_category.cat_index = mus_cat_idx.cat_idx))";
            if ($withimg == 'True') {
              $imageQuery = "SELECT image FROM music_image WHERE mus_index = $dRow[0]";
            }  // End If
            $result = pg_query($database,$dumpQuery);
            $row = @pg_fetch_array($result,0);

            if ($withimg == 'True') {
              $imgresult = pg_query($database, $imageQuery);
              print "  <TABLE>\n";
              if (pg_num_rows($imgresult) > 0) {
                $imageRow = @pg_fetch_array($imgresult,0);
                pg_query ($database, "BEGIN");
                $array = array('images/',$imageRow[0],'.jpg');
                $filename = implode("", $array);
                $var = pg_loexport ($imageRow[0], $filename, $database);
                pg_query ($database, "COMMIT");
                if ($var)
                  print "    <TR><TD VALIGN=TOP><A HREF=\"$filename\">" .
                        "<IMG SRC=\"$filename\" height=140></A></TD>\n";
              } else {
                print "    <TR><TD VALIGN=TOP><IMG SRC=\"images/nocover.jpg\"" .
                      " height=140 width=100></TD>\n";
              }  // End If
              print "    <TD>";
            }  // End If
            print "<TABLE BORDER=1>\n";
            print "  <TR><TD><B>Artist</B></TD><TD>$row[1]</TD></TR>\n";
            print "  <TR><TD><B>Title</B></TD><TD>$row[2]</TD></TR>\n";
            print "  <TR><TD><B>Year</B></TD><TD>$row[4]</TD></TR>\n";
            print "  <TR><TD><B>Category</B></TD><TD>$row[3]</TD></TR>\n";
            print "  <TR><TD><B>Recording Label</B></TD><TD>$row[5]</TD></TR>\n";
            print "  <TR><TD><B>Number of Tracks</B></TD><TD>$row[6]</TD></TR>\n";
            print "  <TR><TD><B>Format</B></TD><TD>$row[7]</TD></TR>\n";
            print "  <TR><TD><B>Notes</B></TD><TD>$row[8]</TD></TR>\n";
            if ($withimg == 'True') {
              print "  </TABLE>\n";
            }  // End If
            print "</TABLE><BR><HR><BR>\n";
          } // FOR Statement: Loop through all records in db 
        } else {   // This is only a retrieval of a single record
            $query = "SELECT mus_index, title, year, recording_label, num_tracks, " .
                     "format, music.notes FROM music, music_label, music_format " .
                     "WHERE ((mus_index = $number) AND " .
                     "(music.lab_index = music_label.lab_index) AND " .
                     "(music.form_index = music_format.form_index))";
            $imageQuery = "SELECT image FROM music_image WHERE mus_index = $number";
            $artquery = "SELECT art_index, artist FROM music, music_artist, " .
                        "mus_art_idx WHERE ((music.mus_index = $number) AND " .
                        "(music.mus_index = mus_art_idx.mus_idx) AND " .
                        "(music_artist.art_index = mus_art_idx.art_idx)) " .
                        "ORDER BY artist";
            $catquery = "SELECT cat_index, category FROM music, music_category, " .
                        "mus_cat_idx WHERE ((music.mus_index = $number) AND " .
                        "(music.mus_index = mus_cat_idx.mus_idx) AND " .
                        "(music_category.cat_index = mus_cat_idx.cat_idx)) " .
                        "ORDER BY category";
            $result = pg_query($database,$query);
            $artresult = pg_query ($database, $artquery);
            $catresult =  pg_query ($database, $catquery);
            $imgresult = pg_query($database, $imageQuery);
            $row = @pg_fetch_array($result,0);
            print "  <TABLE>\n";
            if (pg_num_rows($imgresult) > 0) {
              $imageRow = @pg_fetch_array($imgresult,0);
              pg_query ($database, "BEGIN");
              $array = array('images/',$imageRow[0],'.jpg');
              $filename = implode("", $array);
              $var = pg_loexport ($imageRow[0], $filename, $database);
              pg_query ($database, "COMMIT");
              if ($var)
                print "    <TR><TD VALIGN=TOP><A HREF=\"$filename\">" .
                      "<IMG SRC=\"$filename\" height=140></A></TD>\n";
            } else {
              print "    <TR><TD VALIGN=TOP><IMG SRC=\"images/nocover.jpg\"" .
                    " height=140 width=100></TD>\n";
            }
            print "    <TD><TABLE BORDER=1>\n";
            print "<FORM ACTION=\"music_update.php\" METHOD=\"POST\">\n";
            print "    <TR><TD><B>Artist</B></TD><TD>\n";
            for ($i = 0; $artrow=@pg_fetch_array ($artresult, $i); $i++) {
              print "$artrow[1]";
              if ($i < (pg_numrows ($artresult) - 1))
                print ", ";
            }  // FOR STATEMENT: Loop through Artists
            print "</TD></TR>\n";
            print "    <TR><TD><B>Title</B></TD><TD>$row[1]</TD></TR>\n";
            print "    <TR><TD><B>Year</B></TD><TD>$row[2]</TD></TR>\n";
            print "    <TR><TD><B>Category</B></TD><TD>\n";
            for ($i = 0; $catrow=@pg_fetch_array ($catresult, $i); $i++) {
              print "$catrow[1]";
              if ($i < (pg_numrows ($catresult) - 1))
                print "<BR>";
            }  // FOR STATEMENT: Loop through Categories 
            print "</TD></TR>\n";
            print "    <TR><TD><B>Recording Label</B></TD><TD>$row[3]</TD></TR>\n";
            print "    <TR><TD><B>Number of Tracks</B></TD><TD>$row[4]</TD></TR>\n";
            print "    <TR><TD><B>Format</B></TD><TD>$row[5]</TD></TR>\n";
            print "    <TR><TD><B>Notes</B></TD><TD>$row[6]</TD></TR>\n";
            print "    </TABLE></TD></TR>\n";
            print "  </TABLE>\n";
            print "  <INPUT TYPE=\"hidden\" NAME=\"number\" VALUE=\"$row[0]\">\n";
            print "  <INPUT TYPE=\"submit\" VALUE=\"Update Album Information\"><BR>\n";
            print "</FORM>\n";
        } // IF Statement: Is this a dump or a single record?
      } // FUNCTION: retrieve_music

      function write_form() {

        GLOBAL $PHP_SELF;

      ?>
        <CENTER><H1>PgWebInventory - Retrieve Music</H1></CENTER>
        <FORM ACTION="<?php $PHP_SELF ?>" METHOD="POST">
          <CENTER><TABLE>
            <TR>
              <TD><B>Music Index</B></TD>
              <TD><INPUT TYPE="text" NAME="number" SIZE=3></TD>
            </TR>
            <TR>
              <TD><INPUT TYPE="submit" VALUE="Retrieve Book Information"></TD>
              <TD></TD>
            </TR>
          </TABLE></CENTER>
        </FORM>
      <?php
      } // FUNCTION: write_form
    ?>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="music_listing.php">View List of All Music in the Inventory</A><BR>
    <A HREF="music_entry.php">Enter Music into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
