<html>  
  <head>
    <title>PgWebInventory 2.2 - Book Entry Form</title>
  </head>
  <body>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      // Add additional author or category
      if (($additionalauth == "Y") OR ($additionalcat == "Y")) {
        print "You selected to add an additional author.<BR>\n";
        //Add New Book first, then get info for author.  Index will need to come back from add_new_book
        //
        if ($state != "addMore") {
          $bkindex = add_new_book ($index, $isbn, $authindex, $title, $pubindex, $year, $formindex, $catindex, $edition, $seriesindex, $volume, $notes, $userfile, $strCity, $monOrigCost, $monValue, $intQuantity, $dberror);
        } else {
          $return = add_add_author ($bkindex, $authindex);
          if (! $return)
            print "Error: $dberror<BR>";
        }  // End If
        print "<P><H1>Additional Author for <U>$title</U>:</H1></P>\n";
 
        print "<B>Book Index</B>: $bkindex<BR>\n";
        print "<FORM ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
        print "Author: <SELECT NAME=\"authindex\">\n";
        $result = pg_query ($database, "SELECT * FROM book_author ORDER BY author");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=2&varname=Author&table=book_author\">Enter New Author</A><BR>\n";
        print "<INPUT TYPE=\"checkbox\" VALUE=\"Y\" NAME=\"additionalauth\">Please check here if you want to add an additional author for this book.<BR><BR>\n";
        print "<input type=\"hidden\" name=\"slacker\" value=\"AddAuth\">\n";
        print "<input type=\"hidden\" name=\"bkindex\" value=\"$bkindex\">\n";
        print "<input type=\"hidden\" name=\"state\" value=\"addMore\">\n";
        print "<input type=\"hidden\" name=\"title\" value=\"$title\">\n";
        print "<input type=\"submit\" value=\"Add Author to Book\">\n";
        print "<input type=\"reset\" value=\"Reset Form\">\n";
        print "</FORM>\n";      
      } elseif ($slacker == "AddAuth") { 
        // Check user input here!
        $dberror = "";
        $index = "";
        $return = add_add_author ($bkindex, $authindex);
        if (! $return)
          print "Error: $dberror<BR>";
        print "Author #2: $authindex<BR>\n";
        print "Book Index: $bkindex<BR>\n";
        
      } elseif ($slacker == "NewBook") { 
        // Check user input here!
        $dberror = "";
        $index = "";
        $return = add_new_book ($index, $isbn, $authindex, $title, $pubindex, 
                                $year, $formindex, $catindex, $edition, 
                                $seriesindex, $volume, $notes, $userfile, 
                                $strCity, $monOrigCost, $monValue, $intQuantity,
                                $dberror);
        if (! $return)
          print "Error: $dberror<BR>";
        else
          print "Thank you very much.  " . $title . " added.<BR>";
      } else {
          write_form();
      }  // IF STATEMENT
    
     function add_new_book ($index, $isbn, $authindex, $title, $pubindex, 
                            $year, $formindex, $catindex, $edition, 
                            $seriesindex, $volume, $notes, $userfile,
                            $strCity, $monOrigCost, $monValue, $intQuantity,
                            $dberror) { 

        GLOBAL $database, $filename, $_FILES;

        if (! $database) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        // Generate Book Index Value
        $result = pg_query($database,"SELECT * from book");
        $index = pg_num_rows($result) + 1;

        if ($monOrigCost == NULL)
          $monOrigCost = 0.00;
        if ($monValue == NULL)
          $monValue = 0.00;

        // Insert New Book
        $query_1 = "INSERT INTO book VALUES ($index, '$isbn', '$title', $pubindex, $year, $formindex, $edition, $seriesindex, $volume, '$notes', '$strCity', '$monOrigCost', '$monValue', $intQuantity)";
        print "Insert Query: $query_1<br><Br>\n";
        $query_2 = "INSERT INTO book_auth_idx VALUES ($index, $authindex)";
        $query_3 = "INSERT INTO book_cat_idx VALUES ($index, $catindex)";
        if ( ! pg_query($database, $query_1) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_2) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_3) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT

        // Is there a book cover to upload?
        $uploadedFile = $_FILES['userfile']['tmp_name'];
        $realName = $_FILES['userfile']['name'];
        if (is_uploaded_file($userfile)) {
          pg_query ($database, "BEGIN");
          $tmpfname = "/tmp/uploadFile";
          move_uploaded_file ($userfile, "$tmpfname");
          $foid = pg_lo_import ($database, $tmpfname);
          $imgquery = "INSERT INTO book_image (book_index, image) VALUES ($index, $foid)";
          if ( ! pg_query($database, $imgquery) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          pg_query ($database, "COMMIT");
          unlink ($tmpfname);
        }

        $fp = fopen($filename,'a+');
        fwrite($fp,"$query_1 ;\n$query_2 ;\n$query_3 ;\n");
        if ($imgquery == '') {
        } else {
            fwrite($fp,"$imgquery ;\n");
        }
        fclose($fp);
        return $index;
      }  // FUNCTION:  add_new_book

     function add_add_author ($bkindex, $authindex) { 

        GLOBAL $filename, $database;

        if (! $database) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT

        $query = "INSERT INTO book_auth_idx VALUES ($bkindex, $authindex)";

        if ( ! pg_query($database, $query) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        $fp = fopen($filename,'a+');
        fwrite($fp,"$query ;\n");
        fclose($fp);
        return true;
      }  // FUNCTION:  add_add_author

      function write_form() {

        GLOBAL $database, $PHP_SELF;

        ?>
        <CENTER><H1>PgWebInventory - Book Entry Form</H1></Center>
        <?php
        print "<FORM ENCTYPE=\"multipart/form-data\" ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
        if (! $database)
          die("<B>Couldn\'t connect to $db Database</B>"); 
        ?>
        Title: <INPUT TYPE="text" NAME="title" SIZE=65 MAXSIZE=100><BR>
        Author: <SELECT NAME="authindex">
        <?php
        $result = pg_query ($database, "SELECT * FROM book_author ORDER BY author");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        ?>
        </SELECT>  <A HREF="component_entry.php?ctype=2&varname=Author&table=book_author">Enter New Author</A><BR>
        <INPUT TYPE="checkbox" VALUE="Y" NAME="additionalauth">Please check here if you want to add an additional author for this book.<BR><BR>
        ISBN: <INPUT TYPE="text" NAME="isbn" SIZE=16>&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Quantity: <INPUT TYPE="text" NAME="intQuantity" SIZE=4 VALUE=1><BR>
        Publisher: <SELECT NAME="pubindex">
        <?php
        $result = pg_query ($database, "SELECT * FROM book_publisher ORDER BY publisher");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        ?>
        </SELECT>  <A HREF="component_entry.php?ctype=2&varname=Publisher&table=book_publisher">Enter New Book Publisher</A><BR>
        City Published: <INPUT TYPE="text" NAME="strCity" SIZE=30><BR>
        Year: <INPUT TYPE="text" NAME="year" SIZE=4><BR>
        Format: <SELECT NAME="formindex">
        <?php
        $result = pg_query ($database, "SELECT * FROM book_format ORDER BY format");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Format&table=book_format\">Enter New Book Format</A><BR>\n";
        print "Category: <SELECT NAME=\"catindex\">\n";
        $result = pg_query ($database, "SELECT * FROM book_category ORDER BY category");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Category&table=book_category\">Enter New Book Category</A><BR>\n";
        // print "<INPUT TYPE=\"checkbox\" VALUE=\"Y\" NAME=\"additionalcat\">Please check here if you want to add an additional category this book.<BR><BR>\n";
        print "Edition: <INPUT TYPE=\"text\" NAME=\"edition\" SIZE=3 VALUE=1><BR>\n";
        print "Series: <SELECT NAME=\"seriesindex\">\n";
        $result = pg_query ($database, "SELECT * FROM book_series ORDER BY series");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
            print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
      ?>
      </SELECT>  <A HREF="component_entry.php?ctype=1&varname=Series&table=book_series">Enter New Book Series</A><BR>
      Volume in Series (if applicable, 0 if not): <INPUT TYPE="text" NAME="volume" VALUE="0" SIZE=3><BR>
      Original Cost ($): <INPUT TYPE="text" NAME="monOrigCost" SIZE=7>&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Current Value (if known): <INPUT TYPE="text" NAME="monValue" SIZE=7><BR>
      Notes: <BR><TEXTAREA name="notes" COLS=60 ROWS=6 WRAP=physical></TEXTAREA><BR>
      <INPUT TYPE="hidden" NAME="MAX_FILE_SIZE" VALUE=500000>
      Image of Book Cover (optional):  <INPUT TYPE="file" NAME="userfile"><BR>
      <input type="hidden" name="slacker" value="NewBook">
      <input type="submit" value="Insert Book into Inventory">
      <input type="reset" value="Reset Form">
    </form>
    <?php

      }  // FUNCTION: write_form

    ?>
    <BR><HR><BR>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="book_listing.php?option=all">View List of All Books in the Inventory</A><BR>
    <A HREF="book_entry.php">Enter a New  Book into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
