<HTML>
  <HEAD>
    <TITLE>PgWebInventory 2.2 - Book Retrieve</TITLE>
  </HEAD>
  <BODY>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

     if (isset($number)) 
        retrieve_book ($number, $withimg);
      else 
        write_form();

      function retrieve_book ($number, $withimg) {

        GLOBAL $database;

        print "<CENTER><H1>PgWebInventory - Book Retrieval Results</H1></CENTER>\n";
        print "<BR><HR><BR>\n";
        if ($number == 'dump') { 
          $res_a = pg_query ($database, "SELECT book_index, title FROM book ORDER BY Title");
          for ($i = 0; $dRow=@pg_fetch_array($res_a, $i); $i++) {
            $dumpQuery = "SELECT book_index, isbn, title, publisher, year, format, " .
                         "edition, series, volume, book.notes, strCity, monOrigCost, " .
                         "monValue, intQuantity " .
                         "FROM book, book_publisher, book_format, book_series " .
                         "WHERE ((book.book_index = $dRow[0]) AND " .
                         "(book.pub_index = book_publisher.pub_index) AND " .
                         "(book.form_index = book_format.form_index) AND " .
                         "(book.series_index = book_series.series_index))";
            if ($withimg == 'True') {
              $imageQuery = "SELECT image FROM book_image WHERE book_index = $dRow[0]";
            }  // End If
            $authquery = "SELECT auth_index, author " . 
                         "FROM book, book_auth_idx, book_author " .
                         "WHERE ((book_index = $dRow[0]) AND " .
                         "(book_index = book_idx) AND " .
                         "(auth_index = auth_idx)) " .
                         "ORDER BY author";
            $catquery = "SELECT cat_index, category " .
                        "FROM book, book_category, book_cat_idx " .
                        "WHERE ((book_index = $dRow[0]) AND " .
                        "(book_index = book_idx) AND " .
                        "(cat_index = book_cat_idx)) " .
                        "ORDER BY category";

            $result = pg_query($database, $dumpQuery);
            $authresult = pg_query($database, $authquery);
            $catresult = pg_query($database, $catquery);

            $row = @pg_fetch_array($result,0);

            if ($withimg == 'True') {
              $imgresult = pg_query($database, $imageQuery);
              print "  <TABLE>\n";
              if (pg_num_rows($imgresult) > 0) {
                $imageRow = @pg_fetch_array($imgresult,0);
                pg_query ($database, "BEGIN");
                $array = array('images/',$imageRow[0],'.jpg');
                $filename = implode("", $array);
                $array2 = array('/var/www/Inventory', $filename); 
                $filename2 = implode("", $array2); 
                $var = pg_lo_export ($database, $imageRow[0], $filename);
                pg_query ($database, "END");
                if ($var)
                  print "    <TR><TD VALIGN=TOP><A HREF=\"$filename\">" .
                        "<IMG SRC=\"$filename\" height=140></A></TD>\n";
              } else {
                print "    <TR><TD VALIGN=TOP><IMG SRC=\"images/nocover.jpg\"" .
                      " height=140 width=100></TD>\n";
              }
              print "    <TD>";
            }  // End If
            print "<TABLE BORDER=1>\n";
            print "  <TR><TD><B>Title</B></TD><TD>$row[2]</TD></TR>\n";
            print "<TR><TD><B>Author</B></TD><TD>\n";
            for ($j = 0; $authrow=@pg_fetch_array($authresult, $j);$j++) {
              print "<A HREF=\"book_listing.php?option=author&authnum=" .
                    "$authrow[0]\">$authrow[1]</A>";
              if ($j < (pg_num_rows($authresult) - 1)) // Needs to be updated
                                                      // when transition to PHP 4.2
                print ", ";
            }
            print "</TD></TR>\n";
            print "<TR><TD><B>ISBN</B></TD><TD>$row[1]</TD></TR>\n";
            print "<TR><TD><B>Quantity</B></TD><TD>$row[13]</TD></TR>\n";
            print "<TR><TD><B>Year</B></TD><TD>$row[4]</TD></TR>\n";
            print "<TR><TD><B>Publisher</B></TD><TD>$row[3]</TD></TR>\n";
            print "<TR><TD><B>City Published</B></TD><TD>$row[10]</TD></TR>\n";
            print "<TR><TD><B>Category</B></TD><TD>\n";
            for ($j = 0; $catrow=@pg_fetch_array($catresult, $j); $j++) 
              print "<A HREF=\"book_listing.php?option=category&catnum=" .
                    "$catrow[0]\">$catrow[1]</A><BR>\n";
            print "</TD></TR>\n";
            print "<TR><TD><B>Format</B></TD><TD>$row[5]</TD></TR>\n";
            print "<TR><TD><B>Edition</B></TD><TD>$row[6]</TD></TR>\n";
            print "<TR><TD><B>Series</B></TD><TD>$row[7]</TD></TR>\n";
            print "<TR><TD><B>Volume</B></TD><TD>$row[8]</TD></TR>\n";
            print "<TR><TD><B>Original Cost ($)</B></TD><TD>$row[11]</TD></TR>\n";
            print "<TR><TD><B>Current Value ($)</B></TD><TD>$row[12]</TD></TR>\n";
            print "<TR><TD><B>Notes</B></TD><TD>$row[9]</TD></TR>\n";
            if ($withimg == 'True') {
              print "    </TABLE></TD></TR>\n";
            }  // End If
            print "</TABLE><BR><HR><BR>\n";
          } // FOR Statement: Loop through all records in db 
        } else {   // This is only a retrieval of a single record
            $query = "SELECT book_index, isbn, title, publisher, year, format, " .
                     "edition, series, volume, book.notes, strCity, monOrigCost, " .
                     "monValue, intQuantity, book.pub_index " .
                     "FROM book, book_publisher, book_format, book_series " .
                     "WHERE ((book.book_index = $number) AND " .
                     "(book.pub_index = book_publisher.pub_index) AND " .
                     "(book.form_index = book_format.form_index) AND " .
                     "(book.series_index = book_series.series_index))";
            $imageQuery = "SELECT image FROM book_image WHERE book_index = $number";
            $authquery = "SELECT auth_index, author " . 
                         "FROM book, book_auth_idx, book_author " .
                         "WHERE ((book_index = $number) AND " .
                         "(book_index = book_idx) AND " .
                         "(auth_index = auth_idx)) " .
                         "ORDER BY author";
            $catquery = "SELECT cat_index, category " .
                        "FROM book, book_category, book_cat_idx " .
                        "WHERE ((book_index = $number) AND " .
                        "(book_index = book_idx) AND " .
                        "(cat_index = book_cat_idx)) " . 
                        "ORDER BY category";
            $result = pg_query($database,$query);
            $authresult = pg_query($database, $authquery);
            $catresult = pg_query($database, $catquery);
            $imgresult = pg_query($database, $imageQuery);
            $row = @pg_fetch_array($result,0);
            print "  <TABLE>\n";
            if (pg_num_rows($imgresult) > 0) {
              $imageRow = @pg_fetch_array($imgresult,0);
              pg_query ($database, "BEGIN");
              $array = array('images/',$imageRow[0],'.jpg');
              $filename = implode("", $array);
              $array2 = array('/var/www/Inventory', $filename); 
              $filename2 = implode("", $array2); 
              $var = pg_lo_export ($database, $imageRow[0], $filename);
              pg_query($database, "END");
              if ($var)
                print "    <TR><TD VALIGN=TOP><A HREF=\"$filename\">" .
                      "<IMG SRC=\"$filename\" height=140></A></TD>\n";
              
            } else {
              print "    <TR><TD VALIGN=TOP><IMG SRC=\"images/nocover.jpg\"" .
                    " height=140 width=100></TD>\n";
            }
            print "    <TD><TABLE BORDER=1>\n";
            print "<FORM ACTION=\"book_update.php\" METHOD=\"POST\">\n";
            print "<TR><TD><B>Title</B></TD><TD>$row[2]</TD></TR>\n";
            print "<TR><TD><B>Author</B></TD><TD>\n";
            for ($i = 0; $authrow=@pg_fetch_array($authresult, $i);$i++) {
              print "<A HREF=\"book_listing.php?option=author&authnum=" .
                    "$authrow[0]\">$authrow[1]</A>";
              if ($i < (pg_num_rows($authresult) - 1)) // Needs to be updated
                                                      // when transition to PHP 4.2
                print ", ";
            }
            print "</TD></TR>\n";
            print "<TR><TD><B>ISBN</B></TD><TD>$row[1]</TD></TR>\n";
            print "<TR><TD><B>Quantity</B></TD><TD>$row[13]</TD></TR>\n";
            print "<TR><TD><B>Year</B></TD><TD>$row[4]</TD></TR>\n";
            print "<TR><TD><B>Publisher</B></TD><TD><A HREF=\"book_listing.php?option=publisher&pubnum=$row[14]\">$row[3]</a></TD></TR>\n";
            print "<TR><TD><B>City Published</B></TD><TD>$row[10]</TD></TR>\n";
            print "<TR><TD><B>Category</B></TD><TD>\n";
            for ($i = 0; $catrow=@pg_fetch_array($catresult, $i); $i++) 
              print "<A HREF=\"book_listing.php?option=category&catnum=" .
                    "$catrow[0]\">$catrow[1]</A><BR>\n";
            print "</TD></TR>\n";
            print "<TR><TD><B>Format</B></TD><TD>$row[5]</TD></TR>\n";
            print "<TR><TD><B>Edition</B></TD><TD>$row[6]</TD></TR>\n";
            print "<TR><TD><B>Series</B></TD><TD>$row[7]</TD></TR>\n";
            print "<TR><TD><B>Volume</B></TD><TD>$row[8]</TD></TR>\n";
            print "<TR><TD><B>Original Cost ($)</B></TD><TD>$row[11]</TD></TR>\n";
            print "<TR><TD><B>Current Value ($)</B></TD><TD>$row[12]</TD></TR>\n";
            print "<TR><TD><B>Notes</B></TD><TD>$row[9]</TD></TR>\n";
            print "    </TABLE></TD></TR>\n";
            print "  </TABLE>\n";
            print "<INPUT TYPE=\"hidden\" NAME=\"number\" VALUE=\"$row[0]\">\n";
            print "<INPUT TYPE=\"submit\" VALUE=\"Update Book Information\"><BR>\n";
            print "</FORM>\n";
        }  // End If
      } // FUNCTION: retrieve_book 

      function write_form() {

        GLOBAL $PHP_SELF;

    ?>
    <CENTER><H1>Household Inventory - Retrieve Book</H1></CENTER>
    <FORM ACTION="<?php $PHP_SELF ?>" METHOD="POST">
      <CENTER><TABLE BORDER=3 WIDTH=50%>
        <TR>
          <TD><B>Book Index</B></TD>
          <TD><INPUT TYPE="text" NAME="number" SIZE=3></TD>
          <TD></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD><CENTER><INPUT TYPE="submit" VALUE="Retrieve Book Information"></CENTER></TD>
          <TD></TD>
        </TR>
      </TABLE></CENTER>
    </FORM>
    <?php
      } // FUNCTION: write_form
    ?>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="book_listing.php?option=all">View List of All Books in the Inventory</A><BR>
    <A HREF="book_entry.php">Enter a new Book into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
