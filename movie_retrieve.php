<HTML>
  <HEAD>
    <TITLE>PgWebInventory 2.1 - Movie Retrieve</TITLE>
  </HEAD>
  <BODY>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      if (isset($number)) 
        retrieve_movie ($number, $withimg);
      else 
        write_form();

      function retrieve_movie ($number, $withimg) {

        GLOBAL $database, $PHP_SELF;

        print "<CENTER><H1>PgWebInventory - Movie Retrieval Results</H1></CENTER>\n";
        print "<BR><HR><BR>\n";
        if ($number == 'dump') { 
          $res_a = pg_query ($database, "SELECT mov_index, name FROM movie ORDER BY name");
          for ($i = 0; $dRow=@pg_fetch_array($res_a, $i); $i++) {
            // Build various Queries used by all formats
            $dumpQuery = "SELECT mov_index, name, year, length, rating, synopsis, format " .
                         "FROM movie, movie_rating, movie_format " .
                         "WHERE ((movie.mov_index = $dRow[0]) AND " .
                         "(movie.rat_index = movie_rating.rat_index) AND " .
                         "(movie.form_index = movie_format.form_index))";
            if ($withimg == 'True') {
              $imageQuery = "SELECT image FROM movie_image WHERE mov_index = $dRow[0]";
            }  // End if
            $dirQuery = "SELECT dir_index, director " .
                        "FROM movie, movie_director, mov_dir_idx " .
                        "WHERE ((movie.mov_index = $dRow[0]) AND " .
                        "(movie.mov_index = mov_dir_idx.mov_idx) AND " .
                        "(movie_director.dir_index = mov_dir_idx.dir_idx)) " .
                        "ORDER BY director";
            $actQuery = "SELECT act_index, actor " .
                        "FROM movie, movie_actor, mov_act_idx " .
                        "WHERE ((movie.mov_index = $dRow[0]) AND " .
                        "(movie.mov_index = mov_act_idx.mov_idx) AND " .
                        "(movie_actor.act_index = mov_act_idx.act_idx)) " .
                        "ORDER BY actor";
            $scrQuery = "SELECT scr_index, screen_format " .
                        "FROM movie, movie_screen_format, mov_scr_idx " .
                        "WHERE ((movie.mov_index = $dRow[0]) AND " .
                        "(movie.mov_index = mov_scr_idx.mov_idx) AND " .
                        "(movie_screen_format.scr_index = mov_scr_idx.scr_idx)) " .
                        "ORDER BY screen_format";
            $catQuery = "SELECT cat_index, category " .
                        "FROM movie, movie_category, mov_cat_idx " .
                        "WHERE ((movie.mov_index = $dRow[0]) AND " .
                        "(movie.mov_index = mov_cat_idx.mov_idx) AND " .
                        "(movie_category.cat_index = mov_cat_idx.cat_idx)) " .
                        "ORDER BY category";
            $sndQuery = "SELECT sound_index, sound_option " .
                        "FROM movie, movie_sound, mov_snd_idx " .
                        "WHERE ((movie.mov_index = $dRow[0]) AND " .
                        "(movie.mov_index = mov_snd_idx.mov_idx) AND " .
                        "(movie_sound.sound_index = mov_snd_idx.snd_idx)) " . 
                        "ORDER BY sound_option";
    
            // Run queries
            $result = pg_query($database, $dumpQuery);
            $dirResult = pg_query ($database, $dirQuery);
            $actResult = pg_query ($database, $actQuery);
            $scrResult = pg_query ($database, $scrQuery);
            $catResult = pg_query ($database, $catQuery);
            $sndResult = pg_query ($database, $sndQuery);
    
            $row = @pg_fetch_array($result,0);
            if ($withimg == 'True') {
              $result = pg_query($database, $imageQuery);
              // print "<FORM ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
              print "  <TABLE>\n";
              if (pg_numrows($result) > 0) {
                $imageRow = @pg_fetch_array($result,0);
                pg_query ($database, "BEGIN");
                $array = array('images/',$imageRow[0],'.jpg');
                $filename = implode("", $array);
                $var = pg_lo_export ($database, $imageRow[0], $filename);
                pg_query ($database, "COMMIT");
                if ($var)
                  print "    <TR><TD VALIGN=TOP><A HREF=\"$filename\">" .
                        "<IMG SRC=\"$filename\" height=140 width=100></A></TD>\n";
               
              } else {
                print "    <TR><TD VALIGN=TOP><IMG SRC=\"images/nocover.jpg\"" .
                      " height=140 width=100></TD>\n";
              }
              print "    <TD>";
            }  // End If
            print "<TABLE BORDER=1>\n";
            print "      <TR><TD><B>Title</B></TD><TD>$row[1]</TD></TR>\n";
            print "      <TR><TD><B>Director</B></TD><TD>";
            for ($j = 0; $dirRow=@pg_fetch_array($dirResult, $j); $j++) {
              print "$dirRow[1]";
              if ($j < (pg_num_rows($dirResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Directors
            print "</TD></TR>\n";
            print "      <TR><TD><B>Notable Actors</B></TD><TD>";
            for ($j = 0; $actRow=@pg_fetch_array($actResult, $j); $j++) {
              print "$actRow[1]";
              if ($j < (pg_num_rows($actResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Actors
            print "</TD></TR>\n";
            print "      <TR><TD><B>Year</B></TD><TD>$row[2]</TD>\n";
            print "      <TR><TD><B>Rating</B></TD><TD>$row[4]</TD></TR>\n";
            print "      <TR><TD><B>Format</B></TD><TD>$row[6]</TD></TR>\n";
            print "      <TR><TD><B>Length</B></TD><TD>$row[3]</TD></TR>\n";
            print "      <TR><TD><B>Category</B></TD><TD>";
            for ($j = 0; $catRow=@pg_fetch_array($catResult, $j); $j++) {
              print "$catRow[1]";
              if ($j < (pg_num_rows($catResult) - 1))
                print "<BR>";
            } // FOR STATEMENT: Loop through Categories
            print "</TD></TR>\n";
            print "      <TR><TD><B>Screen Format</B></TD><TD>";
            for ($j = 0; $scrRow=@pg_fetch_array($scrResult, $j); $j++) {
              print "$scrRow[1]";
              if ($j < (pg_num_rows($scrResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Screen Formats
            print "</TD></TR>\n";
            print "      <TR><TD><B>Sound Option(s)</B></TD><TD>";
            for ($j = 0; $sndRow=@pg_fetch_array($sndResult, $j); $j++) {
              print "$sndRow[1]";
              if ($j < (pg_num_rows($sndResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Sound Options
            print "</TD></TR>\n";
          
            // If it is a DVD, display the extra information
            if ($row[6] == "DVD") {
              // Build the DVD-related queries
              $langQuery = "SELECT lang_index, language " .
                           "FROM movie, dvd_languages, mov_lang_idx " .
                           "WHERE ((movie.mov_index = $dRow[0]) AND " .
                           "(movie.mov_index = mov_lang_idx.mov_idx) AND " .
                           "(dvd_languages.lang_index = mov_lang_idx.lang_idx)) " .
                           "ORDER BY language";
              $subQuery = "SELECT sub_index, subtitle " .
                          "FROM movie, dvd_subtitles, mov_sub_idx " .
                          "WHERE ((movie.mov_index = $dRow[0]) AND " .
                          "(movie.mov_index = mov_sub_idx.mov_idx) AND " .
                          "(dvd_subtitles.sub_index = mov_sub_idx.sub_idx)) " .
                          "ORDER BY subtitle";
              $arQuery = "SELECT asp_index, aspect_ratio " .
                         "FROM movie, dvd_aspect_ratio, mov_ar_idx " .
                         "WHERE ((movie.mov_index = $dRow[0]) AND " .
                         "(movie.mov_index = mov_ar_idx.mov_idx) AND " .
                         "(dvd_aspect_ratio.asp_index = mov_ar_idx.ar_idx)) " .
                         "ORDER BY aspect_ratio";
              $sfQuery = "SELECT sf_index, special_feature " .
                         "FROM movie, dvd_special_features, mov_sf_idx " .
                         "WHERE ((movie.mov_index = $dRow[0]) AND " .
                         "(movie.mov_index = mov_sf_idx.mov_idx) AND " .
                         "(dvd_special_features.sf_index = mov_sf_idx.sf_idx)) " .
                         "ORDER BY special_feature";
 
              // Execute the DVD-related queries
              $langResult = pg_query ($database, $langQuery);
              $subResult = pg_query ($database, $subQuery);
              $arResult = pg_query ($database, $arQuery);
              $sfResult = pg_query ($database, $sfQuery);
              print "      <TR><TD><B>Languages on DVD</B></TD><TD>";
              for ($j = 0; $langRow=@pg_fetch_array($langResult, $j); $j++) {
                print "$langRow[1]";
                if ($j < (pg_num_rows($langResult) - 1))
                  print ", ";
              } // FOR STATEMENT: Loop through Languages
              print "</TD></TR>\n";
              print "      <TR><TD><B>Subtitles on DVD</B></TD><TD>";
              for ($j = 0; $subRow=@pg_fetch_array($subResult, $j); $j++) {
                print "$subRow[1]";
                if ($j < (pg_num_rows($subResult) - 1))
                  print ", ";
              } // FOR STATEMENT: Loop through Subtitles
              print "</TD></TR>\n";
              print "      <TR><TD><B>Aspect Ratio on DVD</B></TD><TD>";
              for ($j = 0; $arRow=@pg_fetch_array($arResult, $j); $j++) {
                print "$arRow[1]";
                if ($j < (pg_num_rows($arResult) - 1))
                  print ", ";
              } // FOR STATEMENT: Loop through Aspect Ratios
              print "</TD></TR>\n";
              print "      <TR><TD><B>Special Features on DVD</B></TD><TD>";
              for ($j = 0; $sfRow=@pg_fetch_array($sfResult, $j); $j++) {
                print "$sfRow[1]";
                if ($j < (pg_num_rows($sfResult) - 1))
                  print ", ";
              } // FOR STATEMENT: Loop through Special Features
              print "</TD></TR>\n";
            } // IF STATEMENT: Do only if it is a DVD
     
            print "      <TR><TD><B>Synopsis</B></TD><TD>$row[5]</TD></TR>\n";
            if ($withimg == 'True') {
              print "    </TABLE></TD></TR>\n";
            }  // End If
            print "  </TABLE><BR><HR><BR>\n";
          }  // For Loop
        } else {
          // Build various Queries used by all formats
          $query = "SELECT mov_index, name, year, length, rating, synopsis, format " .
                   "FROM movie, movie_rating, movie_format " .
                   "WHERE ((movie.mov_index = $number) AND " .
                   "(movie.rat_index = movie_rating.rat_index) AND " .
                   "(movie.form_index = movie_format.form_index))";
          $imageQuery = "SELECT image FROM movie_image WHERE mov_index = $number";
          $dirQuery = "SELECT dir_index, director " .
                      "FROM movie, movie_director, mov_dir_idx " . 
                      "WHERE ((movie.mov_index = $number) AND " .
                      "(movie.mov_index = mov_dir_idx.mov_idx) AND " .
                      "(movie_director.dir_index = mov_dir_idx.dir_idx)) " .
                      "ORDER BY director";
          $actQuery = "SELECT act_index, actor " .
                      "FROM movie, movie_actor, mov_act_idx " .
                      "WHERE ((movie.mov_index = $number) AND " .
                      "(movie.mov_index = mov_act_idx.mov_idx) AND " .
                      "(movie_actor.act_index = mov_act_idx.act_idx)) " .
                      "ORDER BY actor";
          $scrQuery = "SELECT scr_index, screen_format " .
                      "FROM movie, movie_screen_format, mov_scr_idx " .
                      "WHERE ((movie.mov_index = $number) AND " .
                      "(movie.mov_index = mov_scr_idx.mov_idx) AND " .
                      "(movie_screen_format.scr_index = mov_scr_idx.scr_idx)) " .
                      "ORDER BY screen_format";
          $catQuery = "SELECT cat_index, category " .
                      "FROM movie, movie_category, mov_cat_idx " .
                      "WHERE ((movie.mov_index = $number) AND " .
                      "(movie.mov_index = mov_cat_idx.mov_idx) AND " .
                      "(movie_category.cat_index = mov_cat_idx.cat_idx)) " .
                      "ORDER BY category";
          $sndQuery = "SELECT sound_index, sound_option " .
                      "FROM movie, movie_sound, mov_snd_idx " . 
                      "WHERE ((movie.mov_index = $number) AND " .
                      "(movie.mov_index = mov_snd_idx.mov_idx) AND " .
                      "(movie_sound.sound_index = mov_snd_idx.snd_idx)) " .
                      "ORDER BY sound_option";
  
          // Run queries
          $result = pg_query($database,$query);
          $dirResult = pg_query ($database, $dirQuery);
          $actResult = pg_query ($database, $actQuery);
          $scrResult = pg_query ($database, $scrQuery);
          $catResult = pg_query ($database, $catQuery);
          $sndResult = pg_query ($database, $sndQuery);
  
          $row = @pg_fetch_array($result,0);
          $result = pg_query($database, $imageQuery);
          // print "<FORM ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
          print "  <TABLE>\n";
          if (pg_numrows($result) > 0) {
            $imageRow = @pg_fetch_array($result,0);
            pg_query ($database, "BEGIN");
            $array = array('images/',$imageRow[0],'.jpg');
            $filename = implode("", $array);
            $var = pg_lo_export ($database, $imageRow[0], $filename);
            pg_query ($database, "COMMIT");
            if ($var)
              print "    <TR><TD VALIGN=TOP><A HREF=\"$filename\">" .
                    "<IMG SRC=\"$filename\" height=140 width=100></A></TD>\n";
           
          } else {
            print "    <TR><TD VALIGN=TOP><IMG SRC=\"images/nocover.jpg\"" .
                  " height=140 width=100></TD>\n";
          }
          print "    <TD><TABLE BORDER=1>\n";
          print "      <TR><TD><B>Title</B></TD><TD>$row[1]</TD></TR>\n";
          print "      <TR><TD><B>Director</B></TD><TD>";
          for ($i = 0; $dirRow=@pg_fetch_array($dirResult, $i); $i++) {
            print "$dirRow[1]";
            if ($i < (pg_numrows($dirResult) - 1))
              print ", ";
          } // FOR STATEMENT: Loop through Directors
          print "</TD></TR>\n";
          print "      <TR><TD><B>Notable Actors</B></TD><TD>";
          for ($i = 0; $actRow=@pg_fetch_array($actResult, $i); $i++) {
            print "$actRow[1]";
            if ($i < (pg_numrows($actResult) - 1))
              print ", ";
          } // FOR STATEMENT: Loop through Actors
          print "</TD></TR>\n";
          print "      <TR><TD><B>Year</B></TD><TD>$row[2]</TD>\n";
          print "      <TR><TD><B>Rating</B></TD><TD>$row[4]</TD></TR>\n";
          print "      <TR><TD><B>Format</B></TD><TD>$row[6]</TD></TR>\n";
          print "      <TR><TD><B>Length</B></TD><TD>$row[3]</TD></TR>\n";
          print "      <TR><TD><B>Category</B></TD><TD>";
          for ($i = 0; $catRow=@pg_fetch_array($catResult, $i); $i++) {
            print "$catRow[1]";
            if ($i < (pg_numrows($catResult) - 1))
              print "<BR>";
          } // FOR STATEMENT: Loop through Categories
          print "</TD></TR>\n";
          print "      <TR><TD><B>Screen Format</B></TD><TD>";
          for ($i = 0; $scrRow=@pg_fetch_array($scrResult, $i); $i++) {
            print "$scrRow[1]";
            if ($i < (pg_numrows($scrResult) - 1))
              print ", ";
          } // FOR STATEMENT: Loop through Screen Formats
          print "</TD></TR>\n";
          print "      <TR><TD><B>Sound Option(s)</B></TD><TD>";
          for ($i = 0; $sndRow=@pg_fetch_array($sndResult, $i); $i++) {
            print "$sndRow[1]";
            if ($i < (pg_numrows($sndResult) - 1))
              print ", ";
          } // FOR STATEMENT: Loop through Sound Options
          print "</TD></TR>\n";
          
          // If it is a DVD, display the extra information
          if ($row[6] == "DVD") {
            $langQuery = "SELECT lang_index, language " .
                         "FROM movie, dvd_languages, mov_lang_idx " .
                         "WHERE ((movie.mov_index = $number) AND " .
                         "(movie.mov_index = mov_lang_idx.mov_idx) AND " .
                         "(dvd_languages.lang_index = mov_lang_idx.lang_idx)) " .
                         "ORDER BY language";
            $subQuery = "SELECT sub_index, subtitle " .
                        "FROM movie, dvd_subtitles, mov_sub_idx " .
                        "WHERE ((movie.mov_index = $number) AND " .
                        "(movie.mov_index = mov_sub_idx.mov_idx) AND " .
                        "(dvd_subtitles.sub_index = mov_sub_idx.sub_idx)) " .
                        "ORDER BY subtitle";
            $arQuery = "SELECT asp_index, aspect_ratio " .
                       "FROM movie, dvd_aspect_ratio, mov_ar_idx " .
                       "WHERE ((movie.mov_index = $number) AND " .
                       "(movie.mov_index = mov_ar_idx.mov_idx) AND " .
                       "(dvd_aspect_ratio.asp_index = mov_ar_idx.ar_idx)) " .
                       "ORDER BY aspect_ratio";
            $sfQuery = "SELECT sf_index, special_feature " .
                       "FROM movie, dvd_special_features, mov_sf_idx " .
                       "WHERE ((movie.mov_index = $number) AND " .
                       "(movie.mov_index = mov_sf_idx.mov_idx) AND " .
                       "(dvd_special_features.sf_index = mov_sf_idx.sf_idx)) " .
                       "ORDER BY special_feature";
            $langResult = pg_query ($database, $langQuery);
            $subResult = pg_query ($database, $subQuery);
            $arResult = pg_query ($database, $arQuery);
            $sfResult = pg_query ($database, $sfQuery);
            print "      <TR><TD><B>Languages on DVD</B></TD><TD>";
            for ($j = 0; $langRow=@pg_fetch_array($langResult, $j); $j++) {
              print "$langRow[1]";
              if ($j < (pg_num_rows($langResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Languages
            print "</TD></TR>\n";
            print "      <TR><TD><B>Subtitles on DVD</B></TD><TD>";
            for ($j = 0; $subRow=@pg_fetch_array($subResult, $j); $j++) {
              print "$subRow[1]";
              if ($j < (pg_num_rows($subResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Subtitles
            print "</TD></TR>\n";
            print "      <TR><TD><B>Aspect Ratio on DVD</B></TD><TD>";
            for ($j = 0; $arRow=@pg_fetch_array($arResult, $j); $j++) {
              print "$arRow[1]";
              if ($j < (pg_num_rows($arResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Aspect Ratios
            print "</TD></TR>\n";
            print "      <TR><TD><B>Special Features on DVD</B></TD><TD>";
            for ($j = 0; $sfRow=@pg_fetch_array($sfResult, $j); $j++) {
              print "$sfRow[1]";
              if ($j < (pg_num_rows($sfResult) - 1))
                print ", ";
            } // FOR STATEMENT: Loop through Special Features
            print "</TD></TR>\n";
  
          } // IF STATEMENT: Do only if it is a DVD
     
          print "      <TR><TD><B>Synopsis</B></TD><TD>$row[5]</TD></TR>\n";
          print "    </TABLE></TD></TR>\n";
          print "  </TABLE><BR><HR><BR>\n";
        }  // End If
          /* print "<INPUT TYPE=\"hidden\" NAME=\"number\" VALUE=\"$row[0]\">\n";
          print "<INPUT TYPE=\"submit\" VALUE=\"Update Movie Information\"><BR>\n";
          print "</FORM>\n"; */ // Uncomment when movie_update.php is complete
      } // FUNCTION: retrieve_movie 
 
      function write_form() {

        GLOBAL $PHP_SELF;

        print "<CENTER><H1>PgWebInventory - Retrieve Movie</H1></CENTER>\n";
        print "      <FORM ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
        print "        <CENTER><TABLE BORDER=3 WIDTH=50%>\n";
        print "        <TR><TD><B>Movie Index</B></TD><TD><INPUT TYPE=\"text\"" .
              " NAME=\"number\" SIZE=3></TD><TD></TD></TR>\n";
        print "        <TR><TD></TD><TD><CENTER><INPUT TYPE=\"submit\"" .
              " VALUE=\"Retrieve Movie Information\"></CENTER></TD><TD></TD></TR>\n";
        print "        </TABLE></CENTER>\n";
        print "      </FORM>\n";
      } // FUNCTION: write_form
    ?>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="movie_listing.php?format=all">View List of All Movies in the Inventory</A><BR>
    <A HREF="movie_entry.php">Enter a new Movie into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
