<html>  
  <head>
    <title>PgWebInventory 2.1 - Movie Entry Form</title>
    <SCRIPT>
      function yes () {

        var today = new Date();
        var expires = new Date();

        expires.setTime(today.getTime() + 100*60*60*24*3);
        if (this.mainform.formindex.options[this.mainform.formindex.selectedIndex].text == "DVD") {
          MMDiv.style.visibility = 'visible';
          setCookie("DVD", "y", expires);
          mainform.DVDLangindex.focus();
        } else {
          setCookie("DVD", "n", expires);
          MMDiv.style.visibility = 'hidden';
        }
      }  // Function: yes()

      function setCookie(name, value, expire) {

        document.cookie = name + "=" + escape(value) + ((expire==null)?"":(";expires =" + expire.toGMTString()));

      } // Function: setCookie(name, value, expire)

      function getCookie(Name) {

        var search = Name + "=";

        if (document.cookie.length > 0) {
          offset = document.cookie.indexOf(search);
          if (offset != -1) {
            offset += search.length;
            end = document.cookie.indexOf(";", offset);
            if (end == -1)
              end = document.cookie.length;
            return unescape(document.cookie.substring(offset, end));
          }
        }
      } // Function: getCookie(Name)

    </SCRIPT>
  </head>
  <body>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      if (isset($slacker)) { 
        // Check user input here!
        $dberror = "";
        $index = "";
        $return = add_to_database ($index, $title, $year, $ratindex, $length, $notes, $dirindex, $actindex, $scrfrmindex, $catindex, $ratindex, $DVDSpecFeatindex, $DVDLangindex, $DVDSubindex, $DVDAspRatindex, $sndindex, $formindex, $DVD, $userfile, $dberror);
        if (! $return)
          print "Error: $dberror<BR>";
        else
          print "Thank you very much.  " . $title . " added.<BR>";
        } // IF STATEMENT
      else {
        write_form();
      }  // IF STATEMENT
    
     function add_to_database ($index, $title, $year, $ratindex, $length, $notes, $dirindex, $actindex, $scrfrmindex, $catindex, $ratindex, $DVDSpecFeatindex, $DVDLangindex, $DVDSubindex, $DVDAspRatindex, $sndindex, $formindex, $DVD, $userfile, $dberror) { 

        GLOBAL $database, $filename, $_FILES;

        // Generate Movie Index Value
        $result = pg_query($database,"SELECT * from movie");
        $index = pg_num_rows($result) + 1;

        // Build Query Statements to INSERT a new movie
        $query_a = "INSERT INTO movie VALUES ($index, '$title', $year, $ratindex, $length, $formindex, '$notes')";
        $query_b = "INSERT INTO mov_dir_idx VALUES ($index, $dirindex)";
        $query_c = "INSERT INTO mov_act_idx VALUES ($index, $actindex)";
        $query_d = "INSERT INTO mov_scr_idx VALUES ($index, $scrfrmindex)";
        $query_e = "INSERT INTO mov_cat_idx VALUES ($index, $catindex)";
        $query_g = "INSERT INTO mov_snd_idx VALUES ($index, $sndindex)";
        if ($DVD == 'y') {
          $query_h = "INSERT INTO mov_sf_idx VALUES ($index, $DVDSpecFeatindex)";
          $query_i = "INSERT INTO mov_lang_idx VALUES ($index, $DVDLangindex)";
          $query_j = "INSERT INTO mov_sub_idx VALUES ($index, $DVDSubindex)";
          $query_k = "INSERT INTO mov_ar_idx VALUES ($index, $DVDAspRatindex)";
        }  // IF STATEMENT - Build DVD Specific Entries

        // INSERT the movie details into the database
        if ( ! pg_query($database, $query_a) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_b) ) {
         $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
       if ( ! pg_query($database, $query_c) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_d) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_e) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ( ! pg_query($database, $query_g) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        if ($DVD == 'y') {
          if ( ! pg_query($database, $query_h) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          if ( ! pg_query($database, $query_i) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          if ( ! pg_query($database, $query_j) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          if ( ! pg_query($database, $query_k) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
        }  // IF STATEMENT - Only store if it is a DVD

        // Is there a movie cover to upload?
        $uploadedFile = $_FILES['userfile']['tmp_name'];
        $realName = $_FILES['userfile']['name'];
        if (is_uploaded_file($userfile)) {
          pg_query ($database, "BEGIN");
          $tmpfname = "/tmp/uploadFile";
          move_uploaded_file ($userfile, "$tmpfname");
          $foid = pg_lo_import ($database, $tmpfname);
          $query_l = "INSERT INTO movie_image (mov_index, image) VALUES ($index, $foid)";
          if ( ! pg_query($database, $query_l) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          pg_query ($database, "COMMIT");
          unlink ($tmpfname);
        }

        // Save the Queries to a storage file for backup purposes
        $fp = fopen($filename,'a+');
        fwrite($fp,"$query_a ;\n");
        fwrite($fp,"$query_b ;\n");
        fwrite($fp,"$query_c ;\n");
        fwrite($fp,"$query_d ;\n");
        fwrite($fp,"$query_e ;\n");
        fwrite($fp,"$query_g ;\n");
        if ($DVD == 'y') {
          fwrite($fp,"$query_h ;\n");
          fwrite($fp,"$query_i ;\n");
          fwrite($fp,"$query_j ;\n");
          fwrite($fp,"$query_k ;\n");
        }  // IF STATEMENT - Only store if it is a DVD
        if ($query_l == '') {
        } else {
            fwrite($fp,"$query_l ;\n");
        }
        fclose($fp);
        return true;  // Exit sucessfully
      }  // FUNCTION:  add_to_database

      function write_form() {

        GLOBAL $database, $PHP_SELF;

        print "<CENTER><H1>PgWebInventory - Movie Entry Form</H1></Center>\n";
        print "<FORM ENCTYPE=\"multipart/form-data\" NAME=\"mainform\" ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
        print "Title: <INPUT TYPE=\"text\" NAME=\"title\" SIZE=60><BR>\n";
        print "Category: <SELECT NAME=\"catindex\">\n";
        $result = pg_query ($database, "SELECT * FROM movie_category ORDER BY category");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Category&table=movie_category\">Enter New Movie Category</A>&nbsp;&nbsp;\n";
        /* print "Number of Categories? <SELECT NAME=\"catnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "Movie Format: <SELECT NAME=\"formindex\" onChange=\"yes()\">\n";
        $result = pg_query ($database, "SELECT * FROM movie_format ORDER BY format");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Format&table=movie_format\">Enter New Movie Format</A><BR>\n";
        print "<DIV ID=\"MMDiv\" style=\"visibility:hidden\">\n";
        print "DVD Language: <SELECT NAME=\"DVDLangindex\">\n";
        $result = pg_query ($database, "SELECT * FROM dvd_languages ORDER BY language");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Language&table=dvd_languages\">Enter New Language</A>&nbsp;&nbsp;\n";
        /* print "Number of DVD Languages? <SELECT NAME=\"DVDLangnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "DVD Subtitles: <SELECT NAME=\"DVDSubindex\">\n";
        $result = pg_query ($database, "SELECT * FROM dvd_subtitles ORDER BY subtitle");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Subtitle&table=dvd_subtitles\">Enter New Subtitle</A>&nbsp;&nbsp;\n";
        /* print "Number of DVD Subtitles? <SELECT NAME=\"DVDSubnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "DVD Special Features: <SELECT NAME=\"DVDSpecFeatindex\">\n";
        $result = pg_query ($database, "SELECT * FROM dvd_special_features ORDER BY special_feature");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Category&table=dvd_special_features\">Enter New Special Feature</A>&nbsp;&nbsp;\n";
        /* print "Number of DVD Special Features? <SELECT NAME=\"SpecFeatnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "DVD Aspect Ratio: <SELECT NAME=\"DVDAspRatindex\">\n";
        $result = pg_query ($database, "SELECT * FROM dvd_aspect_ratio ORDER BY aspect_ratio");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Aspect_Ratio&table=dvd_aspect_ratio\">Enter New Aspect Ratio</A>&nbsp;&nbsp;\n";
        print "</DIV>\n";
        print "Year: <INPUT TYPE=\"text\" NAME=\"year\" SIZE=4><BR>\n";
        print "Director: <SELECT NAME=\"dirindex\">\n";
        $result = pg_query ($database, "SELECT * FROM movie_director ORDER BY director");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=2&varname=Director&table=movie_director\">Enter New Director</A>&nbsp;&nbsp;\n";
        /* print "Number of Directors? <SELECT NAME=\"dirnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "Notable Actors: <SELECT NAME=\"actindex\">\n";
        $result = pg_query ($database, "SELECT * FROM movie_actor ORDER BY actor");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=2&varname=Actor&table=movie_actor\">Enter New Actor</A>&nbsp;&nbsp;\n";
        /* print "Number of Actors? <SELECT NAME=\"actnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "Rating: <SELECT NAME=\"ratindex\">\n";
        $result = pg_query ($database, "SELECT * FROM movie_rating ORDER BY rat_index");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Rating&table=movie_rating\">Enter New Movie Rating</A><BR>\n";
        print "Length (minutes): <INPUT TYPE=\"text\" NAME=\"length\" SIZE=3><BR>\n";
        print "Sound Quality: <SELECT NAME=\"sndindex\">\n";
        $result = pg_query ($database, "SELECT * FROM movie_sound ORDER BY sound_option");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Sound&table=movie_sound\">Enter New Sound Option</A>&nbsp;&nbsp;\n";
        /* print "Number of Sound Options? <SELECT NAME=\"sndnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "Screen Format: <SELECT NAME=\"scrfrmindex\">\n";
        $result = pg_query ($database, "SELECT * FROM movie_screen_format ORDER BY screen_format");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT>  <A HREF=\"component_entry.php?ctype=1&varname=Screen_Format&table=movie_screen_format\">Enter New Screen Format</A>&nbsp;&nbsp;\n";
        /* print "Number of Screen Formats? <SELECT NAME=\"scrfrmnum\">\n";
        print "\t<OPTION VALUE=1 SELECTED>1\n";
        print "\t<OPTION VALUE=2>2\n";
        print "\t<OPTION VALUE=3>3\n";
        print "\t<OPTION VALUE=4>4\n";
        print "\t<OPTION VALUE=5>5\n";
        print "\t<OPTION VALUE=6>6\n";
        print "\t<OPTION VALUE=7>7\n";
        print "\t<OPTION VALUE=8>8\n";
        print "</SELECT><BR>\n"; */
        print "<BR>\n"; // Remove when the previous section is implemented 
        print "Synopsis: <BR><TEXTAREA name=\"notes\" COLS=60 ROWS=6 WRAP=physical></TEXTAREA><BR>\n";
        print "<INPUT TYPE=\"hidden\" NAME=\"MAX_FILE_SIZE\" VALUE=500000>\n";
        print "Image of Movie Cover (optional):  <INPUT TYPE=\"file\" NAME=\"userfile\"><BR>\n";
        print "<input type=\"hidden\" name=\"slacker\" value=\"Y\">\n";
        print "<input type=\"hidden\" name=\"DVD\" value=\"m\">\n";
        print "<input type=\"submit\" value=\"Insert Movie into Inventory\" 
                onClick=\"document.mainform.DVD.value=getCookie('DVD')\">\n";
        print "<input type=\"reset\" value=\"Reset Form\">\n";
        print "</form>\n";
      }  // FUNCTION: write_form

    ?>
    <BR><HR><BR>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="movie_listing.php?format=all">View List of All Movies in the Inventory</A><BR>
    <A HREF="movie_entry.php">Enter a new Movie into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
