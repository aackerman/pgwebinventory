<HTML>
  <HEAD>
    <TITLE>PgWebInventory 2.1</TITLE>
  </HEAD>
  <BODY>
    <CENTER><H1>PgWebInventory 2.1</H1></CENTER>
    <BR><HR><BR>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database):
        die("<B>Couldn\'t connect to $db Database</B>");
      else: ?>
    <H3>Please select your area:</H3>
    <H3>Books</H3>
    <UL>
      <LI><A HREF="book_entry.php">Enter a New Book into the Inventory</A></LI>
      <LI><A HREF="book_listing.php?option=all">List all Books in the Inventory</A></LI>
      <LI><A HREF="book_retrieve.php?number=dump">Dump Book Inventory to Screen</A>
          (<A HREF="book_retrieve.php?number=dump&withimg=True">With Images</A>)</LI>
    </UL><BR>

    <H3>Movies</H3>
    <UL>
      <LI><A HREF="movie_entry.php">Enter a New Movie into the Inventory</A></LI>
      <LI><A HREF="movie_listing.php?format=all">List all Movies in the Inventory</A></LI>
      <UL>
        <LI><A HREF="movie_listing.php?format=dvd">List only DVD's</A></LI>
        <LI><A HREF="movie_listing.php?format=vhs">List only VHS tapes</A></LI>
      </UL>
      <LI><A HREF="movie_retrieve.php?number=dump">Dump Movie Inventory to Screen</A>
          (<A HREF="movie_retrieve.php?number=dump&withimg=True">With Images</A>)</LI>
    </UL><BR>

    <H3>Music</H3>
    <UL>
      <LI><A HREF="music_entry.php">Enter New Music into the Inventory</A></LI>
      <LI><A HREF="music_listing.php">List all Music in the Inventory</A></LI>
      <LI><A HREF="music_retrieve.php?number=dump">Dump Music Inventory to Screen</A>
          (<A HREF="music_retrieve.php?number=dump&withimg=True">With Images</A>)</LI>
    </UL>
    <?php endif; 
  include("overall_footer.php"); 
?>
