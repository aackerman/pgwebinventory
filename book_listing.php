<html>  
  <head>
    <title>PgWebInventory 2.2 - Book Listing</title>
  </head>
  <body>
    <CENTER><H1>PgWebInventory - Book Listing</H1></Center>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      // Build a case statement here which selects which query and display to run.
      switch ($option) {
        case 'all':  // Show all books
          $valueQuery = "SELECT SUM(monorigcost) FROM book";
          $result1 = pg_query($database, $valueQuery);
          if ( ! $result1 ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          $query = "SELECT * FROM book_listing_view";
          $result = pg_query($database, $query);
          if ( ! $result ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          $numbooks = pg_num_rows($result);
          $libValue = pg_fetch_row($result1, 0);
          print "      There are <B>$numbooks</B> books in the Inventory. The value (based " .
                "upon the original cost) of the library is <B>$libValue[0]</B>.<br>";
          print "      <B>All Books in Inventory:</B><BR>\n";
          print "<TABLE BORDER=1>\n";
          print "      <TR>";
          for ($j = 1; $j < (pg_num_fields($result)); $j++) {
            print "<TH>". pg_field_name($result,$j). "</TH>";
          }
          print "<TH>Author</TH>";
          print "</TR>\n";
          for ($i = 0; $row=@pg_fetch_array($result,$i); $i++) { 
            print "      <TR>";
            print "<TD><A HREF=\"book_retrieve.php?number=$row[0]\">$row[1]</A></TD>";
            print "<TD>$row[2]</TD><TD>$row[3]</TD>";
            $authquery = "SELECT auth_index, author " .
                         "FROM book, book_auth_idx, book_author " .
                         "WHERE (book_index = $row[0]) AND " .
                         "      (book_index = book_idx) AND " .
                         "      (auth_index = auth_idx)";
            $authresult = pg_query($database, $authquery);
            print "<TD>";
            for ($k = 0; $authrow = @pg_fetch_array($authresult, $k); $k++) {
              print "<A HREF=\"book_listing.php?option=author&authnum=$authrow[0]\">" .
                    "$authrow[1]</A>";
              if ($k < (pg_num_rows($authresult) - 1)) {
                print ", ";
              } // IF STATEMENT
            } // FOR STATEMENT
            print "</TD></TR>\n";
          } // FOR STATEMENT
          break;
        case 'author':
          $query = "SELECT book_index AS \"Book ID\", title AS \"Title\", " .
                   "       series AS \"Series\", volume AS \"Volume\" " .
                   "FROM book, book_author, book_auth_idx, book_series " .
                   "WHERE (book.series_index = book_series.series_index) AND " .
                   "      (book.book_index = book_auth_idx.book_idx) AND " .
                   "      (book_author.auth_index = book_auth_idx.auth_idx) AND " .
                   "      (book_author.auth_index = $authnum) " .
                   "ORDER BY series, volume, title";
          $authquery = "SELECT author " .
                       "FROM book_author " .
                       "WHERE (auth_index = $authnum)";
          $result = pg_query($database, $query);
          if ( ! $result ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          $authresult = pg_query($database, $authquery);
          $authrow=@pg_fetch_array($authresult, 0);
          print "<H2>Books by $authrow[0]:</H2>";
          print "<TABLE BORDER=1>\n";
          print "      <TR>";
          for ($j = 1; $j < (pg_num_fields($result)); $j++) {
            print "<TH>". pg_field_name($result,$j). "</TH>";
          }
          print "</TR>\n";
          for ($i = 0; $row=@pg_fetch_array($result,$i); $i++) { 
            print "      <TR>";
            print "<TD><A HREF=\"book_retrieve.php?number=$row[0]\">$row[1]</A></TD>";
            if ($row[2] == '0 - No SERIES')
              print "<TD></TD><TD></TD>";
            else 
              print "<TD>$row[2]</TD><TD><CENTER>$row[3]</CENTER></TD>";
            print "</TR>\n";
          } // FOR STATEMENT
          break;
        case 'category':
          $query = "SELECT book_index AS \"Book ID\", title AS \"Title\"," .
                   "       series AS \"Series\", volume AS \"Volume\" " .
                   "FROM book, book_category, book_cat_idx, book_series " .
                   "WHERE (book.series_index = book_series.series_index) AND " .
                   "      (book.book_index = book_cat_idx.book_idx) AND " .
                   "      (book_category.cat_index = book_cat_idx.book_cat_idx) AND " .
                   "      (book_category.cat_index = $catnum) " .
                   "ORDER BY series, volume, title";
          $catquery = "SELECT category " .
                      "FROM book_category " .
                      "WHERE (cat_index = $catnum)";
          $result = pg_query($database, $query);
          if ( ! $result ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          $catresult = pg_query ($database, $catquery);
          $catrow=@pg_fetch_array($catresult, 0);
          print "<H2>Books in Category $catrow[0]:</H2>";
          print "<TABLE BORDER=1>\n";
          print "      <TR>";
          print "<TH>Title</TH><TH>Author</TH><TH>Series</TH><TH>Volume</TH>";
          print "</TR>\n";
          for ($j = 0; $row=@pg_fetch_array($result,$j); $j++) { 
            print "      <TR>";
            print "<TD><A HREF=\"book_retrieve.php?number=$row[0]\">$row[1]</A></TD>";
            $authquery = "SELECT auth_index, author " .
                         "FROM book, book_auth_idx, book_author " .
                         "WHERE (book_index = $row[0]) AND " .
                         "      (book_index = book_idx) AND " .
                         "      (auth_index = auth_idx)";
            $authresult = pg_query($database, $authquery);
            print "<TD>";
            for ($k = 0; $authrow = @pg_fetch_array($authresult, $k); $k++) {
              print "<A HREF=\"book_listing.php?option=author&authnum=$authrow[0]\">" .
                    "$authrow[1]</A>";
              if ($k < (pg_num_rows($authresult) - 1)) {
                print ", ";
              } // IF STATEMENT
            } // FOR STATEMENT
            print "</TD>";
            if ($row[2] == '0 - No SERIES')
              print "<TD></TD><TD></TD>";
            else 
              print "<TD>$row[2]</TD><TD><CENTER>$row[3]</CENTER></TD>";
            print "</TR>\n";
          } // FOR STATEMENT
          break;
        case 'publisher':
          $query = "SELECT book_index AS \"Book ID\", title AS \"Title\"," .
                   "       series AS \"Series\", volume AS \"Volume\" " .
                   "FROM book, book_publisher, book_series " .
                   "WHERE (book.series_index = book_series.series_index) AND " .
                   "      (book.pub_index = book_publisher.pub_index) AND " .
                   "      (book_publisher.pub_index = $pubnum) " .
                   "ORDER BY series, volume, title";
          $pubquery = "SELECT publisher " .
                      "FROM book_publisher " .
                      "WHERE (pub_index = $pubnum)";
          $result = pg_query($database, $query);
          if ( ! $result ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          $pubresult = pg_query($database, $pubquery);
          if ( ! $pubresult ) {
            $dberror = pg_last_error($database);
            return false;
          }  // If Statement
          $pubname=@pg_fetch_array($pubresult, 0);
          print "<H2>Books Published By $pubname[0]:</H2>";
          print "<TABLE BORDER=1>\n";
          print "      <TR>";
          print "<TH>Title</TH><TH>Author</TH><TH>Series</TH><TH>Volume</TH>";
          print "</TR>\n";
          for ($j = 0; $row=@pg_fetch_array($result,$j); $j++) { 
            print "      <TR>";
            print "<TD><A HREF=\"book_retrieve.php?number=$row[0]\">$row[1]</A></TD>";
            $authquery = "SELECT auth_index, author " .
                         "FROM book, book_auth_idx, book_author " .
                         "WHERE (book_index = $row[0]) AND " .
                         "      (book_index = book_idx) AND " .
                         "      (auth_index = auth_idx)";
            $authresult = pg_query($database, $authquery);
            print "<TD>";
            for ($k = 0; $authrow = @pg_fetch_array($authresult, $k); $k++) {
              print "<A HREF=\"book_listing.php?option=author&authnum=$authrow[0]\">" .
                    "$authrow[1]</A>";
              if ($k < (pg_num_rows($authresult) - 1)) {
                print ", ";
              } // IF STATEMENT
            } // FOR STATEMENT
            print "</TD>";
            if ($row[2] == '0 - No SERIES')
              print "<TD></TD><TD></TD>";
            else 
              print "<TD>$row[2]</TD><TD><CENTER>$row[3]</CENTER></TD>";
            print "</TR>\n";
          } // FOR STATEMENT
          break;
        default:
          print "I don't know what to do here.<BR>\n";
      }  // Switch STATEMENT
      print "    </TABLE>\n";

    ?>
    <BR><HR><BR>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="book_listing.php?option=all">View List of All Books in the Inventory</A><BR>
    <A HREF="book_entry.php">Enter a New Book into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
