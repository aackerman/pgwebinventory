<HTML>
  <HEAD>
    <TITLE>PgWebInventory 2.2 - Book Update Page</TITLE>
  </HEAD>
  <BODY>
    <?php
      // Connect to the database using the stored credentials
      require ("./config/config.php");
      $database = pg_Connect ("host=$db_host dbname=$db user=$db_admin password=$db_pass");
      if (! $database)
        die("<B>Couldn\'t connect to $db Database</B>");

      if (isset($slacker)) { 
        // Check user input here!
        $dberror = "";
        $return = update_inventory ($index, $isbn, $authindex, $title, $pubindex,
                                    $year, $formindex, $catindex, $edition,
                                    $seriesindex, $volume, $notes, $userfile,
                                    $strCity, $monOrigCost, $monValue, $intQuantity,
                                    $dberror);
        if (! $return)
          print "Error: $dberror<BR>";
        else
          print "Thank you very much. $title updated.<BR>";
        } // IF STATEMENT
      else {
        write_form($number);
      }  // IF STATEMENT
    
     function update_inventory ($index, $isbn, $authindex, $title, $pubindex,
                                $year, $formindex, $catindex, $edition,
                                $seriesindex, $volume, $notes, $userfile,
                                $strCity, $monOrigCost, $monValue, $intQuantity,
                                $dberror) {

        GLOBAL $filename, $database, $_FILES;

        // Is there a book cover to upload?
        $uploadedFile = $_FILES['userfile']['tmp_name'];
        $realName = $_FILES['userfile']['name'];
        if (is_uploaded_file($userfile)) {
          pg_query ($database, "BEGIN");
          $tmpfname = "/tmp/uploadFile";
          move_uploaded_file ($userfile, "$tmpfname");
          $foid = pg_lo_import ($database, $tmpfname);
          $query_l = "INSERT INTO book_image (book_index, image) VALUES ($index, $foid)";
          if ( ! pg_query($database, $query_l) ) {
            $dberror = pg_last_error($database);
            return false;
          }  // IF STATEMENT
          pg_query ($database, "COMMIT");
          unlink ($tmpfname);
				}

        // Update Book
        $query = "UPDATE book SET isbn='$isbn', title='$title', pub_index=$pubindex, year='$year', form_index=$formindex, edition=$edition, series_index=$seriesindex, volume=$volume, notes='$notes', strcity='$strCity', monorigcost='$monOrigCost', monvalue='$monValue', intquantity=$intQuantity WHERE book_index=$index";
        if ( ! pg_query($database, $query) ) {
          $dberror = pg_last_error($database);
          return false;
        }  // IF STATEMENT
        $fp = fopen($filename,'a+');
        fwrite($fp,"$query ;\n");
        fclose($fp);
        return true;
      }  // FUNCTION:  update_inventory


      function write_form($number) {

        GLOBAL $PHP_SELF, $database;

        print "<CENTER><H1>PgWebInventory - Book Update Form</H1></Center>\n";
        print "<FORM ENCTYPE=\"multipart/form-data\" ACTION=\"$PHP_SELF\" METHOD=\"POST\">\n";
        $result = pg_query ($database, "SELECT * FROM book WHERE (book_index = $number)");
        $current = @pg_fetch_array ($result,0);
        print "Title: <INPUT TYPE=\"text\" NAME=\"title\" SIZE=60 VALUE=\"$current[2]\"><BR>\n";

        // List all authors for this book
        $authresult = pg_query($database, "SELECT * FROM book_auth_idx, book_author WHERE ((book_idx = $number) AND (auth_index = auth_idx))");
        for ($j=0; $authrow=@pg_fetch_array($authresult, $j);$j++) {
          $authnum = $j + 1;
          print "Author $authnum: <SELECT NAME=\"authindex$authnum\">\n";
          $result = pg_query ($database, "SELECT * FROM book_author ORDER BY author");
          for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
            if ($row[0] == $authrow[1])
              print "\t<option value=\"".$row[0]."\" SELECTED>".$row[1]."\n";
            else
              print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
          } // FOR STATEMENT
          print "</SELECT><BR>\n";
        } // FOR STATEMENT

        print "ISBN: <INPUT TYPE=\"text\" NAME=\"isbn\" SIZE=16 VALUE=\"$current[1]\">\n";
        print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        print "&nbsp;&nbsp;&nbsp;";
        print "Quantity: <INPUT TYPE=\"text\" NAME=\"intQuantity\" SIZE=4 VALUE=\"$current[13]\"><BR>\n";
        print "Publisher: <SELECT NAME=\"pubindex\">\n";
        $result = pg_query ($database, "SELECT * FROM book_publisher ORDER BY publisher");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          if ($row[0] == $current[3])
            print "\t<option value=\"".$row[0]."\" SELECTED>".$row[1]."\n";
          else
            print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT><BR>\n";
        print "City Published: <INPUT TYPE=\"text\" NAME=\"strCity\" SIZE=30 VALUE=\"$current[10]\"><BR>\n";
        print "Year: <INPUT TYPE=\"text\" NAME=\"year\" SIZE=4 VALUE=\"$current[4]\"><BR>\n";
        print "Format: <SELECT NAME=\"formindex\">\n";
        $result = pg_query ($database, "SELECT * FROM book_format ORDER BY format");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          if ($row[0] == $current[5])
            print "\t<option value=\"".$row[0]."\" SELECTED>".$row[1]."\n";
          else
            print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT><BR>\n";

        // List all categories for this book
        $catresult = pg_query($database, "SELECT * FROM book_cat_idx, book_category WHERE ((book_idx = $number) AND (cat_index = book_cat_idx))");
        for ($j=0; $catrow=@pg_fetch_array($catresult, $j);$j++) {
          $catnum = $j + 1;
          print "Category $catnum: <SELECT NAME=\"catindex$catnum\">\n";
          $result = pg_query ($database, "SELECT * FROM book_category ORDER BY category");
          for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
            if ($row[0] == $catrow[1])
              print "\t<option value=\"".$row[0]."\" SELECTED>".$row[1]."\n";
            else
              print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
          } // FOR STATEMENT
          print "</SELECT><BR>\n";
        } // FOR STATEMENT

        print "Edition: <INPUT TYPE=\"text\" NAME=\"edition\" SIZE=3 VALUE=\"$current[6]\"><BR>\n";
        print "Series: <SELECT NAME=\"seriesindex\">\n";
        $result = pg_query($database, "SELECT * FROM book_series ORDER BY series");
        for ($i=0; $row=@pg_fetch_array($result,$i); $i++) {
          if ($row[0] == $current[7])
            print "\t<option value=\"".$row[0]."\" SELECTED>".$row[1]."\n";
          else
            print "\t<option value=\"".$row[0]."\">".$row[1]."\n";
        } // FOR STATEMENT
        print "</SELECT><BR>\n";
        print "Volume in Series (if applicable, 0 if not): <INPUT TYPE=\"text\" NAME=\"volume\" SIZE=3 VALUE=\"$current[8]\"><BR>\n";
        print "Original Cost ($): <INPUT TYPE=\"text\" NAME=\"monOrigCost\" SIZE=7 VALUE=\"$current[11]\">";
        print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        print "Current Value (if known): <INPUT TYPE=\"text\" NAME=\"monValue\" SIZE=7 VALUE=\"$current[12]\"><BR>";
        print "Notes: <BR><TEXTAREA name=\"notes\" COLS=60 ROWS=6 WRAP=physical>$current[9]</TEXTAREA><BR>\n";

        // Only allow book cover upload if one does not already exist
        $result = pg_query($database, "SELECT image FROM book_image WHERE book_index = $number");
        if (pg_num_rows($result) == 0) { 
          print "Image of Book Cover (optional):  <INPUT TYPE=\"file\" NAME=\"userfile\"><BR>\n";
          print "<INPUT TYPE=\"hidden\" NAME=\"MAX_FILE_SIZE\" VALUE=500000>\n";
        } else {         // Inform the user the book's cover has already been loaded
          print "Book cover image has already been uploaded.<br>";
        }  // End If
        print "<input type=\"hidden\" name=\"slacker\" value=\"Y\">\n";
        print "<INPUT TYPE=\"hidden\" NAME=\"index\" VALUE=\"$number\">\n";
        print "<input type=\"submit\" value=\"Update Book Information\">\n";
        print "</form>\n";
        pg_close($database);
      }  // FUNCTION: write_form
    ?>    
    <BR><HR><BR>
    <A HREF="index.php">Return to Main Page</A><BR>
    <A HREF="book_listing.php?option=all">View List of All Books in the Inventory</A><BR>
    <A HREF="book_entry.php">Enter a new Book into the Inventory</A><BR>
<?php include("overall_footer.php"); ?>
